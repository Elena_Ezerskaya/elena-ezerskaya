package com.epam.tat.product.mailru.mail.exception;

public class MailRuLetterException extends Error {

    public MailRuLetterException(String message) {
        super(message);
    }
}
