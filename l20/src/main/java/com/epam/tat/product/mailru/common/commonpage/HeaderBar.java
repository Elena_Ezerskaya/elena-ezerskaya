package com.epam.tat.product.mailru.common.commonpage;

import com.epam.tat.framework.ui.Browser;
import org.openqa.selenium.By;

public class HeaderBar {

    public static final By userNameLabel = By.id("PH_user-email");

    public boolean checkUserIsLoggedIn() {
        return Browser.getInstance().isVisibleWithWait(userNameLabel);
    }

    public String getCurrentUserEmail() {
        return Browser.getInstance().getText(userNameLabel);
    }
}
