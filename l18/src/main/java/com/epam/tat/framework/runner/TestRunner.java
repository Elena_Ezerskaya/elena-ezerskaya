package com.epam.tat.framework.runner;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.epam.tat.framework.listeners.SuiteListener;
import com.epam.tat.framework.listeners.TestListener;
import com.epam.tat.framework.listeners.ThreadParamChanger;
import org.apache.log4j.PropertyConfigurator;
import org.testng.ITestNGListener;
import org.testng.TestNG;
//import ru.yandex.qatools.allure.testng.AllureTestListener;

import java.util.List;

public class TestRunner {

    public static void main(String[] args) {
        parseCli(args);
        PropertyConfigurator.configure(Parameters.getInstance().getLog4j());
        TestNG testNG = configureTestNG();
        testNG.run();
        if (testNG.hasFailure()) {
            System.exit(1);
        }
    }

    private static TestNG configureTestNG() {
        TestNG testNG = new TestNG();
        testNG.addListener((ITestNGListener) new SuiteListener());
        testNG.addListener((ITestNGListener) new TestListener());
        testNG.addListener(new ThreadParamChanger());
        List<String> files = Parameters.getInstance().getSuite();
        testNG.setTestSuites(files);
        return testNG;
    }

    private static void  parseCli(String[] args) {
        System.out.println("Parse parameters using JCommander");
        JCommander jCommander = new JCommander(Parameters.getInstance());
        try {
            jCommander.parse(args);
        } catch (ParameterException e) {
            System.out.println(e.getMessage());
            jCommander.usage();
            System.exit(1);
        }
        if (Parameters.getInstance().isHelp()) {
            jCommander.usage();
            System.exit(0);
        }
    }
}

