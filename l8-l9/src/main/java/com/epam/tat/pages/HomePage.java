package com.epam.tat.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends AbstractPage {
    private static final String CREATE_NEW_LETTER_LOCATOR = "//a[@data-name='compose']";
    private static final String INBOX_LOCATOR = "//*[@id='b-nav_folders']//a[@href='/messages/inbox/']";
    private static final String SENT_LOCATOR = "//*[@id='b-nav_folders']//a[@href='/messages/sent/']";
    private static final String DRAFTS_LOCATOR = "//*[@id='b-nav_folders']//a[@href='/messages/drafts/']";
    private static final String TRASH_LOCATOR = "//*[@id='b-nav_folders']//a[@href='/messages/trash/']";

    @FindBy(xpath = CREATE_NEW_LETTER_LOCATOR)
    private WebElement composeLetter;

    @FindBy(xpath = INBOX_LOCATOR)
    private WebElement inboxLink;

    @FindBy(xpath = SENT_LOCATOR)
    private WebElement sentLink;

    @FindBy(xpath = DRAFTS_LOCATOR)
    private WebElement draftsLink;

    @FindBy(xpath = TRASH_LOCATOR)
    private WebElement trashLink;

    public HomePage(WebDriver driver) {
        super(driver);
        new TopPage(driver).getCurrentUserEmail();
    }

    public TopPage getTopPage() {
        return new TopPage(driver);
    }

    public NewLetterPage getNewLetterPage() {
        composeLetter.click();
        return new NewLetterPage(driver);
    }

    public FoldersPage getInboxPage() {
        waitForAppear(inboxLink).click();
        return new FoldersPage(driver, "0");
    }

    public FoldersPage getSentPage() {
        waitForAppear(sentLink).click();
        return new FoldersPage(driver, "500000");
    }

    public FoldersPage getDraftsPage() {
        waitForAppear(draftsLink).click();
        return new FoldersPage(driver, "500001");
    }

    public FoldersPage getTrashPage() {
        driver.navigate().refresh();
        waitForAppear(trashLink).click();
        return new FoldersPage(driver, "500002");
    }
}
