package com.epam.tat.product.mailru.mail.service;

import com.epam.tat.product.mailru.commonpage.HeaderBar;
import com.epam.tat.product.mailru.commonbo.User;
import com.epam.tat.product.mailru.commonexception.MailRuAuthentificationException;
import com.epam.tat.product.mailru.mail.page.LoginPage;

public class AuthentificationService {

    public String login(User user) {
        LoginPage loginPage = new LoginPage().open();
        loginPage.setLogin(user.getEmail())
                .setPassword(user.getPassword())
                .signIn();
        if (loginPage.hasErrorMessage()) {
            return loginPage.getErrorMessage();
        }
        HeaderBar headerBar = new HeaderBar();
        if (headerBar.isLogin()) {
            return headerBar.getCurrentUserEmail();
        }
        return "Is not logged in!";
    }

    public void loginEx(User user) {
        String loginResult = login(user);
        if (!loginResult.equals(user.getEmail())) {
            throw new MailRuAuthentificationException(loginResult);
        }
    }



    //    public void login(User user) {
    //        LoginPage loginPage = new LoginPage().open();
    //        loginPage.setLogin(user.getEmail())
    //                .setPassword(user.getPassword())
    //                .signIn();
    //        if (loginPage.hasErrorMessage()) {
    //            throw new MailRuAuthentificationException(loginPage.getErrorMessage());
    //        }
    //        HeaderBar headerBar = new HeaderBar();
    //        if (!headerBar.isLogin()) {
    //            throw new MailRuAuthentificationException("Is not logged in!");
    //        }
    //    }
}
