package com.epam.tat.framework.utils;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class TempDataUtils {

    private static final int NUMBER_OF_SYMBOLS = 5;

    public static String getUniqueString() {
        return RandomStringUtils.randomAlphabetic(NUMBER_OF_SYMBOLS);
    }

    public static void createFile(String fileName) throws IOException {
        File file = new File(fileName);
        if (file.exists()) {
            throw new IOException("File " + fileName + " exists");
        }
        FileUtils.writeStringToFile(file, getUniqueString(), "UTF8");
    }

    public static void deleteFile(String fileName) {
        File file = new File(fileName);
        if (file.exists()) {
            file.delete();
        }
    }
}
