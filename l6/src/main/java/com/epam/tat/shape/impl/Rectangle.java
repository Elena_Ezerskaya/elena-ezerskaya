package com.epam.tat.shape.impl;

import com.epam.tat.shape.AbstractShape;

public class Rectangle extends AbstractShape {
    static final int HASHCODECONST = 31;
    private int b;

    public Rectangle(int a, int b) throws IllegalArgumentException {
        super(a);
        if (b > 0) {
            this.b = b;
        } else {
            throw new IllegalArgumentException("Invalid argument");
        }
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int area() {
        return a * b;
    }

    public int perimeter() {
        return (a + b) * 2;
    }

    // logic in equals and hashCode supposes that rectangle:2:4 is equal to rectangle:4:2
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Rectangle rectangle = (Rectangle) o;
        return Math.min(a, b) == Math.min(rectangle.a, rectangle.b)
                && Math.max(a, b) == Math.max(rectangle.a, rectangle.b);
    }

    @Override
    public int hashCode() {
        if (a < b) {
            return HASHCODECONST * a + b;
        } else {
            return HASHCODECONST * b + a;
        }
    }

    public String toString() {
        return "Rectangle - " + a + "  " + b + "\n\t"
                + "Area: " + area() + "\n\t"
                + "Perimeter: " + perimeter();
    }
}
