package com.epam.tat.framework.ui;

import com.thoughtworks.selenium.SeleniumException;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static com.epam.tat.DriverTimeouts.IMPLICIT_WAIT;
import static com.epam.tat.DriverTimeouts.PAGE_LOAD;
import static com.epam.tat.framework.utils.HighLightElement.fnHighlightMe;

public final class Browser implements WrapsDriver {

    private static final int ELEMENT_VISIBILITY_TIMEOUT_SECONDS = 20;

    private static Browser instance;

    private WebDriver wrappedWebDriver;

    private Browser() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        wrappedWebDriver = new ChromeDriver();
        wrappedWebDriver.manage().window().maximize();
        wrappedWebDriver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT.getValue(), TimeUnit.SECONDS);
        wrappedWebDriver.manage().timeouts().pageLoadTimeout(PAGE_LOAD.getValue(), PAGE_LOAD.getUnit());
    }

    public static Browser getInstance() {
        if (instance == null) {
            instance = new Browser();
        }
        return instance;
    }

    public void stopBrowser() {
        try {
            getInstance().wrappedWebDriver.quit();
            getInstance().wrappedWebDriver = null;
        } catch (SeleniumException e) {
            e.printStackTrace();
        } finally {
            instance = null;
        }
    }

    @Override
    public WebDriver getWrappedDriver() {
        return wrappedWebDriver;
    }

    public void navigate(String url) {
        wrappedWebDriver.get(url);
    }

    public void click(By by) {
        waitForAppear(by).click();
    }

    public void highlightAndClick(WebElement element) {
        fnHighlightMe(wrappedWebDriver, element);
        element.click();
    }

    public void highlightAndClick(By by) {
        WebElement element = waitForAppear(by);
        highlightAndClick(element);
    }

    public void type(By by, String text) {
        waitForAppear(by);
        wrappedWebDriver
                .findElement(by)
                .sendKeys(text);
    }

    public void clear(By by) {
        wrappedWebDriver
                .findElement(by)
                .clear();
    }

    public String getText(By by) {
        waitForAppear(by, 2);
        return wrappedWebDriver
                .findElement(by)
                .getText();
    }

    public void refresh() {
        wrappedWebDriver
                .navigate()
                .refresh();
    }

    public void uploadFile(String  fileName, By fileInput) {
        waitForPresence(fileInput)
                .sendKeys(new File(fileName).getAbsolutePath());
    }

    private boolean isVisibleWithWait(By by, int timeout) {
        try {
            waitForAppear(by, timeout);
            return wrappedWebDriver.findElement(by).isDisplayed();
        } catch (NoSuchElementException | TimeoutException e) {
            return false;
        }
    }

    public boolean isVisibleWithWait(By by) {
        return isVisibleWithWait(by, ELEMENT_VISIBILITY_TIMEOUT_SECONDS);
    }

    public boolean isVisible(By by) {
        return isVisibleWithWait(by, 1);
    }

    public boolean isPresent(By by) {
        return wrappedWebDriver.findElements(by).size() > 0;
    }

    public WebElement waitForPresence(By by) {
        return new WebDriverWait(wrappedWebDriver, ELEMENT_VISIBILITY_TIMEOUT_SECONDS)
                .until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public WebElement waitForAppear(By by, int timeout) {
        return new WebDriverWait(wrappedWebDriver, timeout)
                .until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public WebElement waitForAppear(By by) {
        return waitForAppear(by, ELEMENT_VISIBILITY_TIMEOUT_SECONDS);
    }

    public void  waitForInvisible(By by) {
        new WebDriverWait(wrappedWebDriver, ELEMENT_VISIBILITY_TIMEOUT_SECONDS)
                .until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    public void switchToFrame(By by) {
        new WebDriverWait(wrappedWebDriver, ELEMENT_VISIBILITY_TIMEOUT_SECONDS)
                .until(ExpectedConditions
                        .frameToBeAvailableAndSwitchToIt(by));
    }

    public void switchToDefaultContent() {
        wrappedWebDriver.switchTo().defaultContent();
    }

    public boolean isAlertPresent() {
        try {
            wrappedWebDriver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException Ex) {
            return false;
        }
    }

    public String getAlertText() {
        return wrappedWebDriver.switchTo().alert().getText();
    }

    public void dragAndDrop(WebElement from, WebElement to) {
        new Actions(wrappedWebDriver)
                .dragAndDrop(from, to)
                .build()
                .perform();
    }

    public void contextClick(WebElement element) {
        new Actions(wrappedWebDriver)
                .contextClick(element)
                .build()
                .perform();
    }
}
