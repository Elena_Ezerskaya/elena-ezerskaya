package com.epam.tat.product.mailru.cloud.page;

import com.epam.tat.framework.ui.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CloudDataPage {

    private static final String FOLDER_LOCATOR_PATTERN
            = "//div[@id='datalist']//div[@data-name='link' and @data-id='/%s']";
    private static final String FOLDER_CHECKBOX_LOCATOR_PATTERN
            = "//div[@id='datalist']//div[@data-name='link' and @data-id='/%s']//div[@class='b-checkbox__box']";
    private static final By confirmMoveButton = By.xpath("//button[@data-name='move']");
    private static final By getPublishLink = By.xpath("//a[@data-name='publish']");
    private static final By publishLinkUrl = By.xpath("//div[@class='publishing__url']/input[@data-name='url']");

    public CloudDataPage clickMoveConformationButton() {
        Browser.getInstance().highlightAndClick(confirmMoveButton);
        Browser.getInstance().waitForInvisible(confirmMoveButton);
        return this;
    }

    private By getByForDataElement(String name) {
        return By.xpath(String.format(FOLDER_LOCATOR_PATTERN, name));
    }

    public WebElement getDataElement(String name) {
        return Browser.getInstance().waitForAppear(getByForDataElement(name));
    }

    public CloudDataPage clickOnDataElement(String name) {
        Browser.getInstance().click(getByForDataElement(name));
        return this;
    }

    public boolean isFolderExists(String folderName) {
        return Browser.getInstance().isVisibleWithWait(getByForDataElement(folderName));
    }

    public void selectFolder(String folderName) {
        Browser.getInstance().highlightAndClick(By.xpath(String.format(FOLDER_CHECKBOX_LOCATOR_PATTERN, folderName)));
    }

    public CloudDataPage dragAndDropFileToFolder(String fileName, String folderName) {
        Browser.getInstance().dragAndDrop(getDataElement(fileName), getDataElement(folderName));
        return this;
    }

    public CloudDataPage contextClickOnElementToShare(String fileName) {
        Browser.getInstance().contextClick(getDataElement(fileName));
        return this;
    }

    public CloudDataPage clickOnGetShareLink() {
        Browser.getInstance().highlightAndClick(getPublishLink);
        return this;
    }

    public String getPublicUrl() {
        return Browser.getInstance().waitForAppear(publishLinkUrl).getAttribute("value");
    }
}
