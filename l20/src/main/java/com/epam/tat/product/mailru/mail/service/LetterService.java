package com.epam.tat.product.mailru.mail.service;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.product.mailru.mail.bo.Letter;
import com.epam.tat.product.mailru.mail.exception.MailRuLetterException;
import com.epam.tat.product.mailru.mail.page.NewLetterPage;
import com.epam.tat.product.mailru.mail.page.LetterHasBeenSentPage;

public class LetterService {

    public void composeLetter(Letter letter) {
        Log.info(String.format("LetterService.composeLetter: %s",
                letter.toString()));
        new NewLetterPage()
                .open()
                .setFieldTo(letter.getTo())
                .setSubject(letter.getSubject())
                .setBody(letter.getBody());
    }

    public void sendLetter(Letter letter) {
        Log.info(String .format("LetterService.sendLetter: %s", letter.toString()));
        NewLetterPage newLetterPage = new NewLetterPage().sendLetter();
        if (newLetterPage.hasAlert()) {
            return;
        }
        if (letter.getBody().equals("")) {
            newLetterPage.confirmSendingLetterWithEmptyBody();
        }
    }

    public String getSentLetterMessage() {
        LetterHasBeenSentPage letterHasBeenSentPage = new LetterHasBeenSentPage();
        if (letterHasBeenSentPage.isSent()) {
            return letterHasBeenSentPage.getLetterSendingConfirmation();
        }
        return "Is not sent!";
    }

    public String getSentLetterErrorMessage() {
        NewLetterPage newLetterPage = new NewLetterPage();
        if (newLetterPage.hasAlert()) {
            return newLetterPage.getErrorMessage();
        }
        return "No error message has been found";
    }

    public void saveLetter() {
        Log.info("LetterService.saveLetter");
        NewLetterPage newLetterPage = new NewLetterPage().saveLetter();
        if (newLetterPage.isSaved()) {
            return;
        }
        throw new MailRuLetterException("Is not saved!");
    }

    public String getSaveLetterMessage() {
        Log.info("LetterService.getSaveLetterMessage");
        NewLetterPage newLetterPage = new NewLetterPage();
        if (newLetterPage.isSaved()) {
            return newLetterPage.getLetterIsSavedConfirmation();
        }
        return "Is not saved!";
    }
}
