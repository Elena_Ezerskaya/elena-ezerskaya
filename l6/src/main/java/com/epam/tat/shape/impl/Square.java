package com.epam.tat.shape.impl;

import com.epam.tat.shape.AbstractShape;

public class Square extends AbstractShape {
    static final int SQUARESIDESQUANTITY = 4;

    public Square(int a) {
            super(a);
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getA() {
        return a;
    }

    public int area() {
        return (int) (Math.pow(a, 2));
    }

    public int perimeter() {
        return SQUARESIDESQUANTITY * a;
    }

    public String toString() {
        return "Square - " + a + "\n\t"
                + "Area: " + area() + "\n\t"
                + "Perimeter: " + perimeter();
    }
}
