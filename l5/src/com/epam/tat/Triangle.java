package com.epam.tat;


public class Triangle implements Shape {
    private int a;

    public Triangle() {
    }

    public Triangle(int a) {
        this.a = a;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }


    public int area() {
        return (int) (Math.sqrt(3) * Math.pow(a, 2)) / 4;
    }

    public int perimeter() {
        return 3 * a;
    }

    public String toString() {
        return "Triangle - " + a + "\n\t" +
                "Area: " + area() + "\n\t" +
                "Perimeter: " + perimeter();
    }
}
