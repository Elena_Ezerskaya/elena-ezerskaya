package com.epam.tat.product.mailru.cloud.service;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.product.mailru.cloud.page.CloudIncorrectLoginPage;
import com.epam.tat.product.mailru.cloud.page.CloudLoginPage;
import com.epam.tat.product.mailru.cloud.page.CloudStartPage;
import com.epam.tat.product.mailru.common.commonbo.User;
import com.epam.tat.product.mailru.common.commonpage.HeaderBar;
import com.epam.tat.product.mailru.common.commonexception.MailRuAuthenticationException;

public class CloudLoginService {

    public String login(User user) {
        Log.info(String.format("CloudLoginService.login: %s", user.toString()));
        CloudLoginPage cloudLoginPage = new CloudStartPage()
                .open()
                .openLoginPage()
                .setLogin(user.getEmail())
                .setPassword(user.getPassword())
                .signIn();
        if (cloudLoginPage.hasFormInputError()) {
            Log.debug("CloudLoginService.login: form input error");
            throw new MailRuAuthenticationException(cloudLoginPage.getFormInputErrorMessage());
        }
        CloudIncorrectLoginPage cloudIncorrectLoginPage = new CloudIncorrectLoginPage();
        if (cloudIncorrectLoginPage.hasLoginIframe()) {
            Log.debug("CloudLoginService.login: login iframe");
            throw new MailRuAuthenticationException(cloudIncorrectLoginPage.getErrorMessage());
        }
        HeaderBar headerBar = new HeaderBar();
        if (headerBar.checkUserIsLoggedIn()) {
            return headerBar.getCurrentUserEmail();
        }
        throw new MailRuAuthenticationException("Is not logged in!");
    }
}
