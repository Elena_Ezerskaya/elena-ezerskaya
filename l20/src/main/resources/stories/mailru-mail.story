Narrative:
In order to check mail.ru functionality
As a user
I want to create/save/send mail and check
that it works correctly

Lifecycle:
Before:
Given Mail.ru login page is open
And I am logged in with correct credentials

Scenario: Check sending letter with all correctly filled fields
Given letter with all correctly filled fields is composed
When I perform sending letter
Then Letter sent confirmation is displayed

Scenario: Check sending letter without subject and body
Given letter without subject and body fields is composed
When I perform sending letter
Then Letter sent confirmation is displayed

Scenario: Check sending letter without properly filled address
Given letter without properly filled address is composed
When I perform sending letter
Then incorrect address error message is displayed

Scenario: Check that letter with all correctly filled fields is present in Inbox and Sent folders
Given letter with all correctly filled fields is composed
When I perform sending letter
Then letter is present in Inbox and Sent folders

Scenario: Check that letter without subject and body is present in Inbox and Sent folders
Given letter without subject and body fields is composed
When I perform sending letter
Then letter is present in Inbox and Sent folders

Scenario: Check that draft mail is saved in Drafts folder
Given letter with all correctly filled fields is composed
When I perform saving letter
Then message "Сохранено в черновиках в.*" is displayed

Scenario: Check that draft mail is deleted from Drafts and Trash folders
Given letter with all correctly filled fields is composed
And letter is saved
When I perform deleting draft mail
Then draft mail is deleted