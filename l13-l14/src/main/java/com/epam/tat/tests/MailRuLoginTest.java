package com.epam.tat.tests;

import com.epam.tat.product.mailru.common.commonbo.User;
import com.epam.tat.product.mailru.common.commonbo.UserFactory;
import com.epam.tat.product.mailru.common.commonexception.MailRuAuthenticationException;
import com.epam.tat.product.mailru.mail.service.AuthenticationService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MailRuLoginTest {

    private static final String INCORRECT_USERNAME_OR_PASSWORD_ER_MESSAGE = "Неверное имя или пароль";
    private static final String CORRECT_USERNAME_EMPTY_PASSWORD_ER_MESSAGE = "Введите пароль";
    private static final String EMPTY_USERNAME_EMPTY_PASSWORD_ER_MESSAGE = "Введите имя ящика";

    private AuthenticationService authenticationService =  new AuthenticationService();

    @Test(description = "Login to mail.ru with correct credentials")
    public void loginToMailWithCorrectCredentials() {
        User user = UserFactory.getUserWithCorrectCredentials();
        Assert.assertEquals(authenticationService.login(user), user.getEmail(), "Is not logged in!");
    }

    @Test(description = "Login to mail.ru with empty username",
            expectedExceptions = MailRuAuthenticationException.class,
            expectedExceptionsMessageRegExp = EMPTY_USERNAME_EMPTY_PASSWORD_ER_MESSAGE
    )
    public void loginToMailWithEmptyCredentials() {
        User user = UserFactory.getUserWithEmptyEmail();
        authenticationService.login(user);
    }

    @Test(description = "Login to mail.ru with correct username and empty password",
            expectedExceptions = MailRuAuthenticationException.class,
            expectedExceptionsMessageRegExp = CORRECT_USERNAME_EMPTY_PASSWORD_ER_MESSAGE
    )
    public void loginToMailWithEmptyPassword() {
        User user = UserFactory.getUserWithCorrectEmailAndEmptyPassword();
        authenticationService.login(user);
    }

    @Test(description = "Login to mail.ru with incorrect username",
            expectedExceptions = MailRuAuthenticationException.class,
            expectedExceptionsMessageRegExp = INCORRECT_USERNAME_OR_PASSWORD_ER_MESSAGE
    )
    public void loginToMailWithIncorrectUsername() {
        User user = UserFactory.getUserWithIncorrectEmail();
        authenticationService.login(user);
    }

    @Test(description = "Login to mail.ru with correct username and incorrect password",
            expectedExceptions = MailRuAuthenticationException.class,
            expectedExceptionsMessageRegExp = INCORRECT_USERNAME_OR_PASSWORD_ER_MESSAGE
    )
    public void loginToMailWithCorrectUsernameAndIncorrectPassword() {
        User user = UserFactory.getUserWithCorrectEmailAndIncorrectPassword();
        authenticationService.login(user);
    }
}
