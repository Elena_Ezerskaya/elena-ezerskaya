package com.epam.tat;

public interface Shape {

    int perimeter();

    int area();
}
