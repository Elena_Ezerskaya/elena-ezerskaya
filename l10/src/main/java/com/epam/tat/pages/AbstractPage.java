package com.epam.tat.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.PageFactory;

import static com.epam.tat.HighLightElement.fnHighlightMe;

public abstract class AbstractPage {
    protected static final int ELEMENT_VISIBILITY_TIMEOUT_SECONDS = 20;

    protected WebDriver driver;

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriver getWebdriver() {
        return driver;
    }

    public WebElement waitForPresence(By locator) {
        return new WebDriverWait(driver, ELEMENT_VISIBILITY_TIMEOUT_SECONDS)
                .until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public boolean isPresence(String xpath) {
        return driver.findElements(By.xpath(xpath)).size() > 0;
    }

    public WebElement waitForAppear(WebElement element) {
        return new WebDriverWait(driver, ELEMENT_VISIBILITY_TIMEOUT_SECONDS)
                .until(ExpectedConditions.visibilityOf(element));
    }

    public WebElement waitForAppear(By locator) {
        return new WebDriverWait(driver, ELEMENT_VISIBILITY_TIMEOUT_SECONDS)
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public boolean isVisible(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public void highlightAndClick(WebElement element) {
        fnHighlightMe(driver, element);
        element.click();
    }
}
