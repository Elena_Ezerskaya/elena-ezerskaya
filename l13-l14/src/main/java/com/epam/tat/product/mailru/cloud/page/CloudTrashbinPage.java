package com.epam.tat.product.mailru.cloud.page;

import com.epam.tat.framework.ui.Browser;
import org.openqa.selenium.By;

public class CloudTrashbinPage {

    private static final By clearTrashbinButton = By.xpath("//*[@id='toolbar']//div[@data-name='clear']");
    private static final  By confirmClearTrashbinButton = By.xpath("//button[@data-name='empty']");
    private static final By folderMessage = By.xpath("//*[@id='datalist']/div[1]/div/div/div");

    public CloudTrashbinPage clearTrashbin() {
        Browser.getInstance().highlightAndClick(clearTrashbinButton);
        Browser.getInstance().highlightAndClick(confirmClearTrashbinButton);
        return this;
    }

    public String getFolderEmptyMessage() {
        return Browser.getInstance().getText(folderMessage);
    }

}
