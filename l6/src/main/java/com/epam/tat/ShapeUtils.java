package com.epam.tat;

import com.epam.tat.shape.Shape;
import com.epam.tat.shape.impl.Circle;
import com.epam.tat.shape.impl.Rectangle;
import com.epam.tat.shape.impl.Square;
import com.epam.tat.shape.impl.Triangle;
import java.util.*;

public class ShapeUtils {

    public static Shape parse(String s) {
        String[] parsedShape = s.split(":");
        Shape result;
        switch (ShapeType.valueOf(parsedShape[0].toUpperCase())) {
            case TRIANGLE:
                result =  new Triangle(Integer.parseInt(parsedShape[1]));
                break;
            case RECTANGLE:
                result =  new Rectangle(Integer.parseInt(parsedShape[1]), Integer.parseInt(parsedShape[2]));
                break;
            case CIRCLE:
                result =  new Circle(Integer.parseInt(parsedShape[1]));
                break;
            case SQUARE:
                result =  new Square(Integer.parseInt(parsedShape[1]));
                break;
            default:
                return null;
        }
        return result;
    }

    public static void printShapes(List<Shape> shapeList) {
        for (Shape shape : shapeList) {
            System.out.println(shape.toString());
        }
    }
}
