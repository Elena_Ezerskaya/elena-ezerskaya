package com.epam.tat.pages.cloudmailru;

import com.epam.tat.pages.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CloudStartPage extends AbstractPage {
    private static final String ENTER_CLOUD_BUTTON_LOCATOR = "//input[@type='button' and @value='Войти в Облако']";

    @FindBy(xpath = ENTER_CLOUD_BUTTON_LOCATOR)
    private WebElement enterCloudButton;

    public CloudStartPage(WebDriver driver) {
        super(driver);
    }

    public CloudLoginPage enterLoginPage() {
        highlightAndClick(enterCloudButton);
        return new CloudLoginPage(driver);
    }
}
