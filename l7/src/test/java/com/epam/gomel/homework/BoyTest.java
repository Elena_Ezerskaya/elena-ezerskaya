package com.epam.gomel.homework;

import org.testng.annotations.*;

import static com.epam.gomel.homework.GirlTest.MIN_RICH_LIMIT;
import static com.epam.gomel.homework.Month.*;
import static org.testng.Assert.*;

@Listeners(TestListener.class)
public class BoyTest {
    static final double MIN_RICH_LIMIT = 1000000;
    static final double NOT_RICH_CONST = 100000;

    private double minRichLimit;

    @DataProvider(name = "Data for isSummerMonth")
    public Object[][] birthdayMonth() {
        return new Object[][] {
                {JANUARY, false},
                {FEBRUARY, false},
                {MARCH, false},
                {APRIL, false},
                {MAY, false},
                {JUNE, true},
                {JULY, true},
                {AUGUST, true},
                {SEPTEMBER, false},
                {OCTOBER, false},
                {NOVEMBER, false},
                {DECEMBER, false}
        };
    }

    @Test(description = "If month = June or July or August should return true",
            dataProvider =  "Data for isSummerMonth")
    public void isSummerMonthTest(Month month, boolean isSummer)  {
        Boy boy = new Boy(month);
        assertEquals(boy.isSummerMonth(), isSummer);
    }

    @Test(description = "If boy's wealth less than parameter method should returns false, otherwise it should be true")
    @Parameters({"month", "wealth"})
    public void isRichTest(Month month, double wealth) {
        Boy boy = new Boy(month, wealth);
        assertEquals(boy.isRich(), true);
        boy = new Boy(month, wealth - 1);
        assertEquals(boy.isRich(), false);
    }

    @Test(description = "Wealth amount should be decreased on spend amount")
    @Parameters("wealth")
    public void spendSomeMoneyTest(double wealth) {
        Boy boy = new Boy(Month.MAY, wealth);
        boy.spendSomeMoney(wealth - 1.0);
        assertEquals(boy.getWealth(), 1.0);
    }

    @Test (description = "Should throw exception if amountForSpending > wealth",
        expectedExceptions = RuntimeException.class,
        expectedExceptionsMessageRegExp = "Not enough money! Requested amount is.*")
    @Parameters("wealth")
    public void spendSomeMoneyExceptionTest(double wealth) {
        Boy boy = new Boy(Month.MAY, wealth);
        boy.spendSomeMoney(wealth + 1);
    }

    @Test(description = "If girl.isPretty = true should returns true, else returns false")
    public void isPrettyGirlFriendTest() {
        Boy boy = new Boy(MAY, NOT_RICH_CONST, new Girl(true));
        assertTrue(boy.isPrettyGirlFriend());
        boy = new Boy(MAY, MIN_RICH_LIMIT, new Girl(false));
        assertFalse(boy.isPrettyGirlFriend());
    }

    @DataProvider(name = "Data for getMood")
    public Object[][] boys() {
        return new Object[][] {
                {MIN_RICH_LIMIT, true, JULY, Mood.EXCELLENT},
                {MIN_RICH_LIMIT, true, JANUARY, Mood.GOOD},
                {MIN_RICH_LIMIT, false, JULY, Mood.NEUTRAL},
                {MIN_RICH_LIMIT, false, JANUARY, Mood.BAD},
                {NOT_RICH_CONST, true, JANUARY, Mood.BAD},
                {NOT_RICH_CONST, false, JULY, Mood.BAD},
                {NOT_RICH_CONST, false, JANUARY, Mood.HORRIBLE},
        };
    }

    @Test(description = "Checks boy's mood",
            dependsOnMethods = {"isRichTest", "isSummerMonthTest", "isPrettyGirlFriendTest"},
            dataProvider =  "Data for getMood")
    public void getMoodTest(double wealth, boolean pretty, Month month, Mood mood) {
        assertEquals(new Boy(month, wealth, new Girl(pretty)).getMood(), mood);
    }
}
