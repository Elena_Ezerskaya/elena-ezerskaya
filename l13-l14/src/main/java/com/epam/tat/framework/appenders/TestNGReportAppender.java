package com.epam.tat.framework.appenders;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;
import org.testng.Reporter;

public class TestNGReportAppender extends AppenderSkeleton {

    @Override
    protected void append(LoggingEvent loggingEvent) {
        Reporter.log(layout.format(loggingEvent)
                .replaceAll("(.+)(file://.+[\\\\/])(\\d+.png)",
                        "$1<a href=\"$2$3\" target=\"blank\">$3</a>"));
    }

    @Override
    public void close() {
    }

    @Override
    public boolean requiresLayout() {
        return true;
    }
}
