package com.epam.tat.product.mailru.common.commonbo;

public class User {

    private String email;

    private String password;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return String.format("email:%s, password:%s", email, password);
    }
}
