package com.epam.tat.shape.impl;

import com.epam.tat.shape.AbstractShape;

public class Circle extends AbstractShape {

    public Circle(int a) {
        super(a);
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int area() {
        return  (int) (Math.PI * Math.pow(a, 2));
    }

    public int perimeter() {
        return (int) (2 * Math.PI * a);
    }

    @Override
    public String toString() {
        return "Circle - " + a + "\n\t"
                + "Area: " + area() + "\n\t"
                + "Perimeter: " + perimeter();
    }
}
