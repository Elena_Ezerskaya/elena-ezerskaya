package com.epam.tat.product.mailru.cloud.page;

import com.epam.tat.framework.ui.Browser;
import org.openqa.selenium.By;

public class CloudLoginPage {
    private static final By loginInput = By.xpath("//input[@name='Login']");
    private static final By passwordInput = By.xpath("//input[@id='ph_password']");
    private static final By submitButton = By.xpath("//input[@type='submit']/..");
    private static final By inputFormMessage =
            By.xpath("//*[@id='x-ph__authForm']//div[@class='x-ph__form__message js-text']");

    public CloudLoginPage setLogin(String login) {
        Browser.getInstance().type(loginInput, login);
        return this;
    }

    public CloudLoginPage setPassword(String password) {
        Browser.getInstance().type(passwordInput, password);
        return this;
    }

    public CloudLoginPage signIn() {
        Browser.getInstance().highlightAndClick(submitButton);
        return this;
    }

    public boolean hasFormInputError() {
        return Browser.getInstance().isVisible(inputFormMessage);
    }

    public String getFormInputErrorMessage() {
        return Browser.getInstance().getText(inputFormMessage);
    }
}
