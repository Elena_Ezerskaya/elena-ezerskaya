package com.epam.tat.framework.listeners;

import com.epam.tat.framework.logging.Log;
import org.testng.ISuite;
import org.testng.ISuiteListener;

public class SuiteListener implements ISuiteListener {
    @Override
    public void onStart(ISuite suite) {
        Log.info(String.format("Suite %s is started", suite.getName()));
    }

    @Override
    public void onFinish(ISuite suite) {
        Log.info(String.format("Suite %s is finished", suite.getName()));
    }
}
