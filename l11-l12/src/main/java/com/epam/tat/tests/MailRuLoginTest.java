package com.epam.tat.tests;

import com.epam.tat.framework.listeners.TestListener;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.product.mailru.commonbo.User;
import com.epam.tat.product.mailru.commonbo.UserFactory;
import com.epam.tat.product.mailru.mail.service.AuthentificationService;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(TestListener.class)
public class MailRuLoginTest {

    private static final String INCORRECT_USERNAME_OR_PASSWORD_ER_MESSAGE = "Неверное имя или пароль";
    private static final String CORRECT_USERNAME_EMPTY_PASSWORD_ER_MESSAGE = "Введите пароль";
    private static final String EMPTY_USERNAME_EMPTY_PASSWORD_ER_MESSAGE = "Введите имя ящика";

    private AuthentificationService authentificationService;

    @BeforeMethod
    private void setUpForLogin() {
        Browser browser = Browser.getInstance();
        authentificationService = new AuthentificationService();
    }

    @Test(description = "Login to mail.ru with correct credentials")
    public void loginToMailWithCorrectCredentials() {
        User user = UserFactory.getUserWithCorrectCredentials();
        Assert.assertEquals(authentificationService.login(user), user.getEmail(), "Is not logged in!");
    }

    @Test(description = "Login to mail.ru with empty username")
    public void loginToMailWithEmptyCredentials() {
        User user = UserFactory.getUserWithEmptyEmail();
        Assert.assertEquals(authentificationService.login(user),
                EMPTY_USERNAME_EMPTY_PASSWORD_ER_MESSAGE, "Invalid error message!");
    }

    @Test(description = "Login to mail.ru with correct username and empty password")
    public void loginToMailWithEmptyPassword() {
        User user = UserFactory.getUserWithCorrectEmailAndEmptyPassword();
        Assert.assertEquals(authentificationService.login(user),
                CORRECT_USERNAME_EMPTY_PASSWORD_ER_MESSAGE, "Invalid error message!");
    }

    @Test(description = "Login to mail.ru with incorrect username")
    public void loginToMailWithIncorrectUsername() {
        User user = UserFactory.getUserWithIncorrectEmail();
        Assert.assertEquals(authentificationService.login(user),
                INCORRECT_USERNAME_OR_PASSWORD_ER_MESSAGE, "Invalid error message!");
    }

    @Test(description = "Login to mail.ru with correct username and incorrect password")
    public void loginToMailWithCorrectUsernameAndIncorrectPassword() {
        User user = UserFactory.getUserWithCorrectEmailAndIncorrectPassword();
        Assert.assertEquals(authentificationService.login(user),
                INCORRECT_USERNAME_OR_PASSWORD_ER_MESSAGE, "Invalid error message!");
    }

    @AfterMethod
    public void killDriver() {
        Browser.getInstance().stopBrowser();
    }
}
