package com.epam.tat.tests;

import com.epam.tat.framework.listeners.TestListener;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.product.mailru.cloud.service.CloudLoginService;
import com.epam.tat.product.mailru.commonbo.User;
import com.epam.tat.product.mailru.commonbo.UserFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(TestListener.class)
public class CloudMailRuLoginTest {

    private static final String EMPTY_CREDENTIALS_ER_MESSAGE =
            "Введите логин и пароль от своего почтового ящика для того, чтобы продолжить работу с сервисом.";
    private static final String INCORRECT_USERNAME_OR_PASSWORD_ER_MESSAGE =
            "Неверное имя пользователя или пароль. Проверьте правильность введенных данных.";

    private CloudLoginService cloudLoginService;

    @BeforeMethod
    private void setUpForLogin() {
        Browser browser = Browser.getInstance();
        cloudLoginService = new CloudLoginService();
    }

    @Test(description = "Login to cloud.mail.ru with correct credentials")
    public void loginToCloudWithCorrectCredentials() {
        User user = UserFactory.getUserWithCorrectCredentials();
        Assert.assertEquals(cloudLoginService.login(user), user.getEmail(), "Is not logged in!");
    }

    @Test(description = "Login to cloud.mail.ru with empty username")
    public void loginToCloudWithEmptyUsername() {
        User user = UserFactory.getUserWithEmptyEmail();
        Assert.assertEquals(cloudLoginService.login(user), EMPTY_CREDENTIALS_ER_MESSAGE, "Invalid error message!");
    }

    @Test(description = "Login to cloud.mail.ru with correct username and empty password")
    public void loginToCloudWithCorrectUsernameAndEmptyPassword() {
        User user = UserFactory.getUserWithCorrectEmailAndEmptyPassword();
        Assert.assertEquals(cloudLoginService.login(user), EMPTY_CREDENTIALS_ER_MESSAGE, "Invalid error message!");
    }

    @Test(description = "Login to cloud.mail.ru with incorrect username")
    public void loginToCloudWithIncorrectUsername() {
        User user = UserFactory.getUserWithIncorrectEmail();
        Assert.assertEquals(cloudLoginService.login(user),
                INCORRECT_USERNAME_OR_PASSWORD_ER_MESSAGE, "Invalid error message!");
    }

    @Test(description = "Login to cloud.mail.ru with correct username and incorrect password")
    public void loginToCloudWithCorrectUsernameAndIncorrectPassword() {
        User user = UserFactory.getUserWithCorrectEmailAndIncorrectPassword();
        Assert.assertEquals(cloudLoginService.login(user),
                INCORRECT_USERNAME_OR_PASSWORD_ER_MESSAGE, "Invalid error message!");
    }

    @AfterMethod
    public void killDriver() {
        Browser.getInstance().stopBrowser();
    }
}
