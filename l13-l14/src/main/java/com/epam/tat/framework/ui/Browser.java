package com.epam.tat.framework.ui;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.runner.CommonTestRuntimeException;
import com.thoughtworks.selenium.SeleniumException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.epam.tat.DriverTimeouts.IMPLICIT_WAIT;
import static com.epam.tat.DriverTimeouts.PAGE_LOAD;
import static com.epam.tat.framework.utils.HighLightElement.fnHighlightMe;

public final class Browser implements WrapsDriver {

    private static final int ELEMENT_VISIBILITY_TIMEOUT_SECONDS = 20;

    private static ThreadLocal<Browser> instance = new ThreadLocal<>();

    private WebDriver wrappedWebDriver;

    private Browser() {
        Log.debug("Create browser");
        wrappedWebDriver = WebDriverFactory.getWebDriver();
        wrappedWebDriver.manage().window().maximize();
        wrappedWebDriver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT.getValue(), TimeUnit.SECONDS);
        wrappedWebDriver.manage().timeouts().pageLoadTimeout(PAGE_LOAD.getValue(), PAGE_LOAD.getUnit());
    }

    public static synchronized Browser getInstance() {
        if (instance.get() == null) {
            instance.set(new Browser());
        }
        return instance.get();
    }

    public void stopBrowser() {
        Log.debug("Stop browser");
        try {
            getInstance().wrappedWebDriver.quit();
            getInstance().wrappedWebDriver = null;
        } catch (SeleniumException e) {
            e.printStackTrace();
        } finally {
            instance.set(null);
        }
    }

    @Override
    public WebDriver getWrappedDriver() {
        return wrappedWebDriver;
    }

    public void navigate(String url) {
        Log.debug("open " + url);
        wrappedWebDriver.get(url);
    }

    public void click(By by) {
        waitForAppear(by).click();
    }

    public void highlightAndClick(WebElement element) {
        fnHighlightMe(wrappedWebDriver, element);
        element.click();
    }

    public void highlightAndClick(By by) {
        WebElement element = waitForAppear(by);
        Log.debug("click on " + by);
        highlightAndClick(element);
    }

    public void type(By by, String text) {
        Log.debug("type " + text);
        waitForAppear(by);
        wrappedWebDriver
                .findElement(by)
                .sendKeys(text);
    }

    public void clear(By by) {
        Log.debug("clear " + by);
        wrappedWebDriver
                .findElement(by)
                .clear();
    }

    public String getText(By by) {
        waitForAppear(by, 2);
        Log.debug("get text " + by);
        return wrappedWebDriver
                .findElement(by)
                .getText();
    }

    public void refresh() {
        wrappedWebDriver
                .navigate()
                .refresh();
    }

    public void uploadFile(File  file, By fileInput) {
        Log.debug("waitForPresence " + fileInput);
        waitForPresence(fileInput)
                .sendKeys(file.getAbsolutePath());
    }

    private boolean isVisibleWithWait(By by, int timeout) {
        Log.debug("waitForElementIsVisible '" + by + "', timeout: " + timeout + " sec");
        try {
            waitForAppear(by, timeout);
            return wrappedWebDriver.findElement(by).isDisplayed();
        } catch (NoSuchElementException | TimeoutException e) {
            return false;
        }
    }

    public boolean isVisibleWithWait(By by) {
        return isVisibleWithWait(by, ELEMENT_VISIBILITY_TIMEOUT_SECONDS);
    }

    public boolean isVisible(By by) {
        return isVisibleWithWait(by, 2);
    }

    public boolean isPresent(By by) {
        Log.debug("checkElementIsPresent " + by);
        return wrappedWebDriver.findElements(by).size() > 0;
    }

    public WebElement waitForPresence(By by) {
        Log.debug("waitForElementIsPresent " + by);
        return new WebDriverWait(wrappedWebDriver, ELEMENT_VISIBILITY_TIMEOUT_SECONDS)
                .until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public WebElement waitForAppear(By by, int timeout) {
        Log.debug("waitForElementIsAppeared '" + by + "', timeout: " + timeout + " sec");
        return new WebDriverWait(wrappedWebDriver, timeout)
                .until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public WebElement waitForAppear(By by) {
        return waitForAppear(by, ELEMENT_VISIBILITY_TIMEOUT_SECONDS);
    }

    public boolean waitForInvisible(By by) {
        Log.debug("waitForElementIsNotVisible " + by);
        return new WebDriverWait(wrappedWebDriver, ELEMENT_VISIBILITY_TIMEOUT_SECONDS)
                .until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    public void switchToFrame(By by) {
        Log.debug("switchToFrame " + by);
        new WebDriverWait(wrappedWebDriver, ELEMENT_VISIBILITY_TIMEOUT_SECONDS)
                .until(ExpectedConditions
                        .frameToBeAvailableAndSwitchToIt(by));
    }

    public void switchToDefaultContent() {
        Log.debug("switchToDefaultContent");
        wrappedWebDriver.switchTo().defaultContent();
    }

    public boolean isAlertPresent() {
        Log.debug("checkAlertIsPresent");
        try {
            wrappedWebDriver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException Ex) {
            return false;
        }
    }

    public String getAlertText() {
        Log.debug("getAlertText");
        Alert alert = wrappedWebDriver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();
        return alertText;
    }

    public void dragAndDrop(WebElement from, WebElement to) {
        Log.debug("gragAndDrop");
        fnHighlightMe(wrappedWebDriver, from);
        fnHighlightMe(wrappedWebDriver, to);
        new Actions(wrappedWebDriver)
                .dragAndDrop(from, to)
                .build()
                .perform();
    }

    public void contextClick(WebElement element) {
        fnHighlightMe(wrappedWebDriver, element);
        new Actions(wrappedWebDriver)
                .contextClick(element)
                .build()
                .perform();
    }

    public File screenshot() {
        File screenshotFile = new File("screenshots/" + System.nanoTime() + ".png");
        try {
            byte[] screenshotBytes = ((TakesScreenshot) wrappedWebDriver).getScreenshotAs(OutputType.BYTES);
            FileUtils.writeByteArrayToFile(screenshotFile, screenshotBytes);
            Log.info("Screenshot taken: file://" + screenshotFile.getAbsolutePath());
            return screenshotFile;
        } catch (IOException e) {
            throw new CommonTestRuntimeException("Failed to write screenshot: ", e);
        }
    }
}
