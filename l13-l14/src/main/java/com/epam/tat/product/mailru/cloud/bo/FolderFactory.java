package com.epam.tat.product.mailru.cloud.bo;

import com.epam.tat.framework.utils.TempDataUtils;

public class FolderFactory {

    public static Folder getFolder() {
        return new Folder("Folder_" + TempDataUtils.getUniqueString());
    }

    public static String generateFileName() {
        return "File_" + TempDataUtils.getUniqueString() + ".txt";
    }
}
