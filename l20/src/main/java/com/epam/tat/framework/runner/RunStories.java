package com.epam.tat.framework.runner;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.epam.tat.framework.logging.Log;
import com.epam.tat.steps.AfterSteps;
import com.epam.tat.steps.LoginSteps;
import com.epam.tat.steps.MailSteps;
import io.tapack.allure.jbehave.AllureReporter;
import org.apache.log4j.PropertyConfigurator;
import org.jbehave.core.Embeddable;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;

import java.util.List;

import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;
import static org.jbehave.core.reporters.Format.*;

public class RunStories extends JUnitStories {
    private static String[] savedArgs = new String[0];

    public RunStories() {
        parseCli(savedArgs);
        PropertyConfigurator.configure(Parameters.getInstance().getLog4j());
        configuredEmbedder().embedderControls()
                .doGenerateViewAfterStories(true)
                .doIgnoreFailureInStories(false)
                .doIgnoreFailureInView(false);
    }

    @Override
    public Configuration configuration() {
        Class<? extends Embeddable> embeddableClass = this.getClass();
        return new MostUsefulConfiguration()
            .useStoryLoader(new LoadFromClasspath(embeddableClass))
            .useStoryReporterBuilder(new StoryReporterBuilder()
            .withCodeLocation(CodeLocations.codeLocationFromClass(embeddableClass))
            .withDefaultFormats()
            .withFormats(CONSOLE, TXT, HTML)
            .withReporters(new AllureReporter()));
    }

    @Override
    public InjectableStepsFactory stepsFactory() {
        Log.info("stepsFactory");
        return new InstanceStepsFactory(configuration(),
                new LoginSteps(),
                new AfterSteps(),
                new MailSteps());
    }

    @Override
    protected List<String> storyPaths() {
        Log.info("storyPaths");
        return new StoryFinder()
                .findPaths(codeLocationFromClass(this.getClass()),
                        "**/*.story",
                        "**/*.story-exclude");
    }

    private static void  parseCli(String[] args) {
        System.out.println("Parse parameters using JCommander");
        JCommander jCommander = new JCommander(Parameters.getInstance());
        try {
            jCommander.parse(args);
        } catch (ParameterException e) {
            System.out.println(e.getMessage());
            jCommander.usage();
            System.exit(1);
        }
        if (Parameters.getInstance().isHelp()) {
            jCommander.usage();
            System.exit(0);
        }
    }

    public static void main(String[] args) throws Throwable {
        System.out.println("Started from main");
        savedArgs = args;
        new RunStories().run();
    }
}
