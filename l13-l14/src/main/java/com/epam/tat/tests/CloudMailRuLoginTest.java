package com.epam.tat.tests;

import com.epam.tat.product.mailru.cloud.service.CloudLoginService;
import com.epam.tat.product.mailru.common.commonbo.User;
import com.epam.tat.product.mailru.common.commonbo.UserFactory;
import com.epam.tat.product.mailru.common.commonexception.MailRuAuthenticationException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CloudMailRuLoginTest {

    private static final String EMPTY_CREDENTIALS_ER_MESSAGE =
            "Введите логин и пароль от своего почтового ящика для того, чтобы продолжить работу с сервисом.";
    private static final String INCORRECT_USERNAME_OR_PASSWORD_ER_MESSAGE =
            "Неверное имя пользователя или пароль. Проверьте правильность введенных данных.";
    private CloudLoginService cloudLoginService = new CloudLoginService();

    @Test(description = "Login to cloud.mail.ru with correct credentials")
    public void loginToCloudWithCorrectCredentials() {
        User user = UserFactory.getUserWithCorrectCredentials();
        Assert.assertEquals(cloudLoginService.login(user), user.getEmail(), "Is not logged in!");
    }

    @Test(description = "Login to cloud.mail.ru with empty username",
            expectedExceptions = MailRuAuthenticationException.class,
            expectedExceptionsMessageRegExp = EMPTY_CREDENTIALS_ER_MESSAGE
    )
    public void loginToCloudWithEmptyUsername() {
        User user = UserFactory.getUserWithEmptyEmail();
        cloudLoginService.login(user);
    }

    @Test(description = "Login to cloud.mail.ru with correct username and empty password",
            expectedExceptions = MailRuAuthenticationException.class,
            expectedExceptionsMessageRegExp = EMPTY_CREDENTIALS_ER_MESSAGE
    )
    public void loginToCloudWithCorrectUsernameAndEmptyPassword() {
        User user = UserFactory.getUserWithCorrectEmailAndEmptyPassword();
        cloudLoginService.login(user);
    }

    @Test(description = "Login to cloud.mail.ru with incorrect username",
            expectedExceptions = MailRuAuthenticationException.class,
            expectedExceptionsMessageRegExp = INCORRECT_USERNAME_OR_PASSWORD_ER_MESSAGE
    )
    public void loginToCloudWithIncorrectUsername() {
        User user = UserFactory.getUserWithIncorrectEmail();
        cloudLoginService.login(user);
    }

    @Test(description = "Login to cloud.mail.ru with correct username and incorrect password",
            expectedExceptions = MailRuAuthenticationException.class,
            expectedExceptionsMessageRegExp = INCORRECT_USERNAME_OR_PASSWORD_ER_MESSAGE
    )
    public void loginToCloudWithCorrectUsernameAndIncorrectPassword() {
        User user = UserFactory.getUserWithCorrectEmailAndIncorrectPassword();
        cloudLoginService.login(user);
    }
}
