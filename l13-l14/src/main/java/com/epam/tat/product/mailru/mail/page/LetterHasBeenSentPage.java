package com.epam.tat.product.mailru.mail.page;

import com.epam.tat.framework.ui.Browser;
import org.openqa.selenium.By;

public class LetterHasBeenSentPage {

    private static final By letterWasSentConfirmationArea = By.id("b-compose__sent");
    private static final By letterWasSentConfirmation =
            By.xpath("//*[@id='b-compose__sent']//div[@class='message-sent__title']");

    public boolean isSent() {
        return Browser.getInstance().isVisibleWithWait(letterWasSentConfirmationArea);
    }

    public String getLetterSendingConfirmation() {
        return Browser.getInstance().getText(letterWasSentConfirmation);
    }

}
