package com.epam.tat;


public enum ShapeType {
    TRIANGLE, CIRCLE, SQUARE, RECTANGLE
}
