package com.epam.tat.steps;

import com.epam.tat.product.mailru.common.commonbo.User;
import com.epam.tat.product.mailru.common.commonbo.UserFactory;
import com.epam.tat.product.mailru.mail.page.LoginPage;
import com.epam.tat.product.mailru.mail.service.AuthenticationService;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class LoginSteps {
    private AuthenticationService authenticationService =  new AuthenticationService();

    @Given("Mail.ru login page is open")
    public void openMailRuLoginPage() {
        new LoginPage().open();
    }

    @Given("I am logged in with correct credentials")
    @When("I perform login with correct credentials")
    public void tryToLoginWithCorrectCredentials() {
        authenticationService.tryToLogin(UserFactory.getUserWithCorrectCredentials());
    }

    @When("I try to login with incorrect credentials <username>:<password>")
    public void tryToLoginWithIncorrectCredentials(
            @Named("username")String paramUsername,
            @Named("password")String paramPassword) {
        authenticationService.tryToLogin(new User(
                UserFactory.getUsername(paramUsername),
                UserFactory.getPassword(paramPassword)));
    }

    @Then("I am logged in")
    public void isLoggedIn() {
        Assert.assertEquals("Is not logged in!",
                authenticationService.getLoginResult(),
                UserFactory.getUserWithCorrectCredentials().getEmail());
    }

    @Then("error message \"<errormsg>\" is displayed")
    public void isDisplayedErrorMessage(@Named("errormsg")String errormsg) {
        Assert.assertEquals(authenticationService.getLoginResult(), errormsg);
    }
}
