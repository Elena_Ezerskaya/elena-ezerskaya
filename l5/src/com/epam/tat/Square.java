package com.epam.tat;


public class Square implements Shape {
    private int a;

    public Square() {
    }

    public Square(int a) {
        this.a = a;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int area() {
        return (int) (Math.pow(a, 2));
    }

    public int perimeter() {
        return (int) (4 * a);
    }

    public String toString() {
        return "Square - " + a + "\n\t" +
                "Area: " + area() + "\n\t" +
                "Perimeter: " + perimeter();
    }

}
