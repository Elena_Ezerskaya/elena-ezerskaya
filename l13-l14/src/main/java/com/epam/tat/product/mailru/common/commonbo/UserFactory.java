package com.epam.tat.product.mailru.common.commonbo;

import com.epam.tat.framework.utils.TempDataUtils;

public class UserFactory {
    private static final String CORRECT_EMAIL = "elenkha2017@mail.ru";
    private static final String CORRECT_PASSWORD = "ktyrfgtyrf71";

    public static User getUserWithCorrectCredentials() {
        return new User(CORRECT_EMAIL, CORRECT_PASSWORD);
    }

    public static User getUserWithIncorrectCredentials() {
        return new User("random" + TempDataUtils.getUniqueString() + "mail.com",
                "pass" + TempDataUtils.getUniqueString());
    }

    public static User getUserWithIncorrectEmail() {
        return new User("random" + TempDataUtils.getUniqueString()
                + "mail.com", CORRECT_PASSWORD);
    }

    public static User getUserWithCorrectEmailAndIncorrectPassword() {
        return new User(CORRECT_EMAIL, "pass" + TempDataUtils.getUniqueString());
    }

    public static User getUserWithEmptyEmail() {
        return new User("", "");
    }

    public static User getUserWithCorrectEmailAndEmptyPassword() {
        return new User(CORRECT_EMAIL, "");
    }

}
