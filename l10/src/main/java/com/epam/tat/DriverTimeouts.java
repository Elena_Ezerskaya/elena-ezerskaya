package com.epam.tat;

import java.util.concurrent.TimeUnit;

public enum DriverTimeouts {
    IMPLICIT_WAIT(0, TimeUnit.SECONDS),
    PAGE_LOAD(90, TimeUnit.SECONDS);

    private int value;
    private TimeUnit unit;

    DriverTimeouts(int value, TimeUnit unit) {
        this.value = value;
        this.unit = unit;
    }

    public int getValue() {
        return value;
    }

    public TimeUnit getUnit() {
        return unit;
    }
}
