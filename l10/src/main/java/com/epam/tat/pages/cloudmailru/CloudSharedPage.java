package com.epam.tat.pages.cloudmailru;

import com.epam.tat.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CloudSharedPage extends AbstractPage {
    private static final String SHARED_ITEM_LOCATOR = "//div[@id='public-file']/div/div/div/div"
            + "//a[@data-name='link-action']";

    public CloudSharedPage(WebDriver driver) {
        super(driver);
    }

    public boolean isSharedItemPresense(String name) {
        return waitForPresence(By.xpath(SHARED_ITEM_LOCATOR)).getText().equals(name);
    }
}
