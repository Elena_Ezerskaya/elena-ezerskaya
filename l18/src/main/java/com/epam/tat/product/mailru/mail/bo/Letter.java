package com.epam.tat.product.mailru.mail.bo;

public class Letter {

    private String to;

    private String subject;

    private String body;

    public Letter(String to, String subject, String body) {
        this.to = to;
        this.subject = subject;
        this.body = body;
    }

    public String getTo() {
        return to;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    @Override
    public String toString() {
        return String.format("to:%s, subject:%s, body:%s", to, subject, body);
    }

}
