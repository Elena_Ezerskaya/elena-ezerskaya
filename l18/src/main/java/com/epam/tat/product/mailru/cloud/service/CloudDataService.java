package com.epam.tat.product.mailru.cloud.service;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.utils.TempDataUtils;
import com.epam.tat.product.mailru.cloud.bo.Folder;
import com.epam.tat.product.mailru.cloud.exception.CloudDataException;
import com.epam.tat.product.mailru.cloud.page.CloudDataPage;
import com.epam.tat.product.mailru.cloud.page.CloudHomePage;

import java.io.File;
import java.io.IOException;

public class CloudDataService {

    public void clearCloud() {
        if (new CloudDataPage().isFolderEmpty()) {
            return;
        }
        new CloudHomePage()
                .clickOnSelectAll()
                .clickOnDeleteButton()
                .confirmDeletion()
                .switchToTrashbin()
                .clearTrashbin();
    }

    public void   createFolder(Folder folder) {
        Log.info(String.format("CloudDataService.createFolder %s", folder.getName()));
        CloudDataPage cloudDataPage = new CloudHomePage()
                .clickOnCreateButton()
                .chooseCreateFolder()
                .setFolderName(folder.getName())
                .addFolder();
        if (!cloudDataPage.isFolderExists(folder.getName())) {
            throw new CloudDataException("Is not created!");
        }
    }

    public String removeFolder(Folder folder) {
        Log.info(String.format("CloudDataService.removeFolder %s", folder.getName()));
        new CloudDataPage().selectFolder(folder.getName());
        CloudHomePage cloudHomePage = new CloudHomePage()
                .clickOnDeleteButton()
                .confirmDeletion();
        String emptyFolderMessage = cloudHomePage.switchToTrashbin()
                .clearTrashbin()
                .getFolderEmptyMessage();
        if (cloudHomePage.switchToCloudHome().isFolderExists(folder.getName())) {
            throw new CloudDataException("Folder is not removed from /");
        }
        return emptyFolderMessage;
    }

    public void uploadFile(File file) {
        Log.info(String.format("CloudDataService.uploadFile %s", file.getName()));
        try {
            TempDataUtils.createFile(file);
        } catch (IOException e) {
            throw new CloudDataException("Temp file is not created");
        }
        CloudDataPage cloudDataPage = new CloudHomePage()
                .clickOnUploadButton()
                .uploadFile(file)
                .closeUploadPanel();
        if  (!cloudDataPage.isFolderExists(file.getName())) {
            throw new CloudDataException("Is not uploaded!");
        }
        TempDataUtils.deleteFile(file);
    }

    public void dragAndDropFileToFolder(File file, Folder folder) {
        Log.info(String.format("CloudDataService.dragAndDropFileToFolder %s to %s", file.getName(), folder.getName()));
        CloudDataPage cloudDataPage = new CloudDataPage()
                .dragAndDropFileToFolder(file.getName(), folder.getName())
                .clickMoveConformationButton();
        Log.debug("check that file " + file.getName() + "is removed from /");
        if (cloudDataPage.isFolderExists(file.getName())) {
            throw new CloudDataException("Is not removed from / !");
        }
        cloudDataPage.clickOnDataElement(folder.getName());
        Log.debug("check that file " + file.getName() + "is placed to folder " + folder.getName());
        if (!cloudDataPage.isFolderExists(folder.getName() + '/' + file.getName())) {
            throw new CloudDataException("Is not placed into folder !");
        }
    }

    public void shareLinkToElement(String fileName) {
        Log.info(String.format("CloudDataService.shareLinkToElement %s", fileName));
        CloudDataPage cloudDataPage = new CloudDataPage();
        String url = cloudDataPage.contextClickOnElementToShare(fileName)
                .clickOnGetShareLink()
                .getPublicUrl();
        Browser.getInstance().getWrappedDriver().get(url);
    }
}
