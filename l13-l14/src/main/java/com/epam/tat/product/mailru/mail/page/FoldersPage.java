package com.epam.tat.product.mailru.mail.page;

import com.epam.tat.framework.ui.Browser;
import org.openqa.selenium.*;

public class FoldersPage {

    private static final By inboxLink = By.xpath("//*[@id='b-nav_folders']//a[@href='/messages/inbox/']");
    private static final By sentLink = By.xpath("//*[@id='b-nav_folders']//a[@href='/messages/sent/']");
    private static final By draftsLink = By.xpath("//*[@id='b-nav_folders']//a[@href='/messages/drafts/']");
    private static final By trashLink = By.xpath("//*[@id='b-nav_folders']//a[@href='/messages/trash/']");
    private static final String EXACT_SUBJECT_LETTER_LOCATOR_PATTERN =
            "//*[@id='b-letters']//div[@data-cache-key and not(contains(@style,'display'))]//a[@data-subject='%s']";
    private static final String EXACT_SUBJECT_LETTER_LOCATOR_CHECKBOX_PATTERN =
        "//*[@id='b-letters']//div[@data-cache-key and not(contains(@style,'display'))]//a[@data-subject='%s']/div[1]";
    private static final By emptySubjectLetter =
            By.xpath("//*[@id='b-letters']//div[@data-cache-key "
                    + "and not(contains(@style,'display'))]"
                    + "//div[@data-id][1]//a[not(@data-subject) or @data-subject='<Без темы>']");
    private static final String SPECIFIC_FOLDER_LOCATOR_PATTERN =
            "//*[@id='b-letters']//div[@data-cache-key='%s_undefined_false' "
                    + "and not(contains(@style,'display: none'))]";
    private static final By removeButton
            = By.xpath("//*[@id='b-toolbar__right']/div[not(contains(@style, 'display: none;'))]"
            + "//div[@data-name='remove']");

    public boolean isLetterExist(String subject) {
        return Browser.getInstance().isVisibleWithWait(
                By.xpath(String.format(EXACT_SUBJECT_LETTER_LOCATOR_CHECKBOX_PATTERN, subject)));
    }

    public boolean isLetterExist() {
        return Browser.getInstance().isVisibleWithWait(emptySubjectLetter);
    }

    public boolean isLetterNotExist(String subject) {
        return Browser.getInstance().waitForInvisible(
                By.xpath(String.format(EXACT_SUBJECT_LETTER_LOCATOR_CHECKBOX_PATTERN, subject)));
    }

    public FoldersPage selectLetter(String subject) {
        Browser.getInstance().highlightAndClick(By.xpath(String.format(
                EXACT_SUBJECT_LETTER_LOCATOR_CHECKBOX_PATTERN, subject)));
        return this;
    }

    private FoldersPage waitForFolderIsReady(String folderId) {
        Browser.getInstance().waitForPresence(By.xpath(String.format(SPECIFIC_FOLDER_LOCATOR_PATTERN, folderId)));
        return this;
    }

    public FoldersPage switchToInbox() {
        Browser.getInstance().refresh();
        Browser.getInstance().highlightAndClick(inboxLink);
        return waitForFolderIsReady("0");
    }

    public FoldersPage switchToSent() {
        Browser.getInstance().highlightAndClick(sentLink);
        return waitForFolderIsReady("500000");
    }

    public FoldersPage switchToDrafts() {
        Browser.getInstance().highlightAndClick(draftsLink);
        return waitForFolderIsReady("500001");
    }

    public FoldersPage switchToTrash() {
        Browser.getInstance().refresh();
        Browser.getInstance().highlightAndClick(trashLink);
        return waitForFolderIsReady("500002");
    }

    public FoldersPage deleteSelectedLetter() {
        Browser.getInstance().highlightAndClick(removeButton);
        return this;
    }

}


