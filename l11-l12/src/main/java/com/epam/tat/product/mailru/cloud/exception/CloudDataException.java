package com.epam.tat.product.mailru.cloud.exception;

public class CloudDataException extends Error {

    public CloudDataException(String message) {
        super(message);
    }

}
