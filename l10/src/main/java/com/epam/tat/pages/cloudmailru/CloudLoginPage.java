package com.epam.tat.pages.cloudmailru;

import com.epam.tat.pages.AbstractPage;
import com.epam.tat.pages.TopPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CloudLoginPage extends AbstractPage {
    private static final String LOGIN_INPUT_LOCATOR = "//input[@name='Login']";
    private static final String PASSWORD_INPUT_LOCATOR = "//input[@id='ph_password']";
    private static final String LOGIN_BUTTON_SUBMIT_LOCATOR = "//input[@type='submit']/..";

    @FindBy(xpath = LOGIN_INPUT_LOCATOR)
    private WebElement loginInput;

    @FindBy(xpath = PASSWORD_INPUT_LOCATOR)
    private WebElement loginPassword;

    @FindBy(xpath = LOGIN_BUTTON_SUBMIT_LOCATOR)
    private  WebElement submit;

    public CloudLoginPage(WebDriver driver) {
        super(driver);
    }

    public TopPage login(String email, String pass) {
        tryLogin(email, pass);
        return new TopPage(driver);
    }

    private void tryLogin(String email, String pass) {
        waitForAppear(loginInput).sendKeys(email);
        loginPassword.sendKeys(pass);
        highlightAndClick(submit);
    }
}
