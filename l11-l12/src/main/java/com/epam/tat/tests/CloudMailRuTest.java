package com.epam.tat.tests;

import com.epam.tat.framework.listeners.TestListener;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.product.mailru.cloud.bo.Folder;
import com.epam.tat.product.mailru.cloud.bo.FolderFactory;
import com.epam.tat.product.mailru.cloud.service.CloudDataService;
import com.epam.tat.product.mailru.cloud.service.CloudLoginService;
import com.epam.tat.product.mailru.commonbo.User;
import com.epam.tat.product.mailru.commonbo.UserFactory;
import com.epam.tat.product.mailru.cloud.page.CloudSharedPage;
import org.testng.Assert;
import org.testng.annotations.*;

@Listeners(TestListener.class)
public class CloudMailRuTest {

    private CloudDataService cloudDataService;

    @BeforeMethod
    private void setUp() {
        //Browser browser = Browser.getInstance();
        CloudLoginService cloudLoginService = new CloudLoginService();
        User user = UserFactory.getUserWithCorrectCredentials();
        cloudLoginService.loginEx(user);
        cloudDataService = new CloudDataService();
    }

    @Test(description = "Create new folder and check that it created")
    public void createNewFolderAndCheckItCreated() {
        Folder folder = FolderFactory.generateFolderName();
        Assert.assertTrue(cloudDataService.createFolder(folder), "Folder is not created!");
    }

    @Test(description = "Check that removed folder does not exist")
    public void removeFolderAndCheckItDoesNotExist() {
        Folder folder = FolderFactory.generateFolderName();
        cloudDataService.createFolder(folder);
        Assert.assertTrue(cloudDataService.removeFolder(folder), "Folder is not removed!");
    }

    @Test(description = "Upload file to root directory and check that it is uploaded")
    public void uploadFileToRootandCheckItUploaded() {
        String fileName = FolderFactory.generateFileName();
        Assert.assertTrue(cloudDataService.uploadFile(fileName), "File is not uploaded!");
    }

    @Test(description = "Check that uploaded file is replaced to folder")
    public void checkThatUploadedFileIsReplacedInNewFolder() {
        Folder folder = FolderFactory.generateFolderName();
        String fileName = FolderFactory.generateFileName();
        cloudDataService.createFolder(folder);
        cloudDataService.uploadFile(fileName);
        Assert.assertTrue(cloudDataService.dragAndDropFileToFolder(fileName, folder), "File is not replaced!");
    }

    @Test(description = "Share link, check that link opens and displays shared element")
    public void createShareAndCheckIt() {
        String fileName = FolderFactory.generateFileName();
        cloudDataService.uploadFile(fileName);
        cloudDataService.shareLinkToElement(fileName);
        Assert.assertTrue(new CloudSharedPage().isSharedItemPresense(fileName), "Shared link is not opened!");
    }

    @AfterMethod
    public void killDriver() {
        Browser.getInstance().stopBrowser();
    }
}
