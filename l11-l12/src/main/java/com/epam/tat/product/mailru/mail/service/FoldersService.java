package com.epam.tat.product.mailru.mail.service;

import com.epam.tat.product.mailru.mail.bo.Letter;
import com.epam.tat.product.mailru.mail.page.FoldersPage;

public class FoldersService {

    public boolean isLetterPresentInInboxAndSent(Letter letter) {
        FoldersPage foldersPage = new FoldersPage();
        if (letter.getSubject().equals("")) {
            return foldersPage.switchToInbox()
                    .isLetterExist()
                    &&
                    foldersPage.switchToSent()
                            .isLetterExist();
        } else {
            return foldersPage.switchToInbox()
                    .isLetterExist(letter.getSubject())
                    &&
                    foldersPage.switchToSent()
                            .isLetterExist(letter.getSubject());
        }
    }

    public boolean deleteLetterFromDraftAndTrash(Letter letter) {
        FoldersPage foldersPage = new FoldersPage();
        return  foldersPage.switchToDrafts()
                .selectLetter(letter.getSubject())
                .deleteSelectedLetter()
                .switchToTrash()
                .selectLetter(letter.getSubject())
                .deleteSelectedLetter()
                .isLetterExist(letter.getSubject());
    }
}
