package com.epam.tat.product.mailru.mail.service;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.product.mailru.common.commonpage.HeaderBar;
import com.epam.tat.product.mailru.common.commonbo.User;
import com.epam.tat.product.mailru.common.commonexception.MailRuAuthenticationException;
import com.epam.tat.product.mailru.mail.page.LoginPage;

public class AuthenticationService {

    public void tryToLogin(User user) {
        Log.info(String.format("AuthenticationService.tryToLogin: %s", user.toString()));
        LoginPage loginPage = new LoginPage();
        loginPage.setLogin(user.getEmail())
                .setPassword(user.getPassword())
                .signIn();
    }

    public String getLoginResult() {
        LoginPage loginPage = new LoginPage();
        if (loginPage.hasErrorMessage()) {
            Log.debug("AuthenticationService.getLoginResult: has error message");
            return loginPage.getErrorMessage();
        }
        HeaderBar headerBar = new HeaderBar();
        if (headerBar.checkUserIsLoggedIn()) {
            Log.debug(headerBar.getCurrentUserEmail());
            return headerBar.getCurrentUserEmail();
        }
        throw new MailRuAuthenticationException("Is not logged in!");
    }
}
