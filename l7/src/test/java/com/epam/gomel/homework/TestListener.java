package com.epam.gomel.homework;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

public class TestListener implements IInvokedMethodListener {
    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        System.out.println(
                method.getTestMethod().getInstance().getClass().getSimpleName()
                + "."
                + method.getTestMethod().getMethodName());
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        String result;
        if (testResult.getStatus() == 1) {
            result = "Success";
        } else {
            result = "Fail";
        }
        System.out.println(String.format("%s.%s >>> %s",
                method.getTestMethod().getInstance().getClass().getSimpleName(),
                method.getTestMethod().getMethodName(),
                result));
    }
}


