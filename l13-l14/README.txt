Run notes:

1. Use run.bat to run tests from command line.

2. Use following "Program arguments" to run it from IDE:
  -s test-suites/mailru-login.xml,test-suites/mailru-cloud.xml,test-suites/mailru-mail.xml
  -l src/main/resources/log4j.properties
  -d src/main/resources/chromedriver.exe

3. Suite mailru-all.xml contains all required tests with paralleling by tests.

4. Using --parallel option overrides "parallel" and "thread-count" settings in suite xml.