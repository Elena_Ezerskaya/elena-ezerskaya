package com.epam.tat.steps;

import com.epam.tat.product.mailru.mail.bo.Letter;
import com.epam.tat.product.mailru.mail.bo.LetterFactory;
import com.epam.tat.product.mailru.mail.service.FoldersService;
import com.epam.tat.product.mailru.mail.service.LetterService;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class MailSteps {
    private static final String LETTER_IS_SENT_CONFIRMATION = "Ваше письмо отправлено. Перейти во Входящие";
    private static final String INCORRECT_ADDRESS_ALERT_ER_MESSAGE =
            "В поле «Кому» указан некорректный адрес получателя.\n"
                    + "Исправьте ошибку и отправьте письмо ещё раз.";
    private static final String LETTER_IS_SAVED_CONFIRMATION = "Сохранено в черновиках в.*";

    private LetterService letterService = new LetterService();
    private FoldersService foldersService = new FoldersService();
    private Letter letter;

    @Given("letter with all correctly filled fields is composed")
    public void composeCorrectLetter() {
        letter = LetterFactory.getLetterWithCorrectToSubjectBody();
        letterService.composeLetter(letter);
    }

    @Given("letter without subject and body fields is composed")
    public void composeLetterWithoutSubjectAndBodyr() {
        letter = LetterFactory.getLetterWithoutSubjectAndBody();
        letterService.composeLetter(letter);
    }

    @Given("letter without properly filled address is composed")
    public void composeLetterWithInvalidFieldTo() {
        letter = LetterFactory.getLetterWithInvalidFieldTo();
        letterService.composeLetter(letter);
    }

    @When("I perform sending letter")
    public void sendLetter() {
        letterService.sendLetter(letter);
    }

    @Given("letter is saved")
    @When("I perform saving letter")
    public void saveDraft() {
        letterService.saveLetter();
    }

    @When("I perform deleting draft mail")
    public void deleteDraft() {
        foldersService.deleteLetterFromDraftAndTrash(letter);
    }

    @Then("Letter sent confirmation is displayed")
    public void getLetterSentMessageText() {
        Assert.assertEquals("Letter is not sent!",
                LETTER_IS_SENT_CONFIRMATION,
                letterService.getSentLetterMessage());
    }

    @Then("incorrect address error message is displayed")
    public void getLetterSentErrorMessageText() {
        Assert.assertEquals("Error message is not displayed",
                INCORRECT_ADDRESS_ALERT_ER_MESSAGE,
                letterService.getSentLetterErrorMessage());
    }

    @Then("letter is present in Inbox and Sent folders")
    public void isLetterPresentInInboxAndSentBox() {
        Assert.assertTrue("Letter is not present in Inbox and Sent folders!",
                foldersService.isLetterPresentInInboxAndSent(letter));
    }

    @Then("message \"Сохранено в черновиках в.*\" is displayed")
    public void isLetterSaved() {
        Assert.assertTrue("Draft mail is not saved!",
                letterService.getSaveLetterMessage().matches(LETTER_IS_SAVED_CONFIRMATION));
    }

    @Then("draft mail is deleted")
    public void isDraftDeleted() {
        Assert.assertTrue("Draft mail is not deleted!",
                foldersService.isDraftNotExist(letter));
    }
}
