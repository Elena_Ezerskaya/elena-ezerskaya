package com.epam.tat.product.mailru.mail.service;

import com.epam.tat.product.mailru.mail.bo.Letter;
import com.epam.tat.product.mailru.mail.exception.MailRuLetterException;
import com.epam.tat.product.mailru.mail.page.NewLetterPage;
import com.epam.tat.product.mailru.mail.page.LetterHasBeenSentPage;

public class LetterService {

    private NewLetterPage newLetterPage;

    public void composeLetter(Letter letter) {
        newLetterPage = new NewLetterPage().open();
        newLetterPage.setFieldTo(letter.getTo())
                .setSubject(letter.getSubject())
                .setBody(letter.getBody());
    }

    public String sendLetter(Letter letter) {
        newLetterPage.sendLetter();
        if (newLetterPage.hasAlert()) {
            return newLetterPage.getErrorMessage();
        }
        if (letter.getBody().equals("")) {
            newLetterPage.confirmSendingLetterWithEmptyBody();
        }
        LetterHasBeenSentPage letterHasBeenSentPage = new LetterHasBeenSentPage();
        if (letterHasBeenSentPage.isSent()) {
            return letterHasBeenSentPage.getLetterSendingConfirmation();
        }
        return "Is not sent!";
    }

    public String saveLetter(Letter letter) {
        newLetterPage.saveLetter();
        if (newLetterPage.isSaved()) {
            return newLetterPage.getLetterIsSavedConfirmation();
        } else {
            throw new MailRuLetterException("Is not saved!");
        }
    }
}
