package com.epam.tat.shape;

public abstract class AbstractShape implements Shape {
    protected int a;

    public AbstractShape(int a) throws IllegalArgumentException {
        if (a > 0) {
            this.a = a;
        } else {
            throw new IllegalArgumentException("Invalid argument");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AbstractShape shape = (AbstractShape) o;
        return a == shape.a;
    }

    @Override
    public int hashCode() {
        return a;
    }
}
