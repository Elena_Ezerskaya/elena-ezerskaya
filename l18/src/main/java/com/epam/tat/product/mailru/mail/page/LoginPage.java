package com.epam.tat.product.mailru.mail.page;

import com.epam.tat.framework.ui.Browser;
import org.openqa.selenium.By;

public class LoginPage {
    private static final String URL = "https://mail.ru/";

    private static final By loginInput = By.id("mailbox:login");
    private static final By passwordInput = By.id("mailbox:password");
    private static final By loginButton = By.id("mailbox:submit");
    private static final By errorMessage = By.id("mailbox:error");

    public LoginPage open() {
        Browser.getInstance().navigate(URL);
        return this;
    }

    public LoginPage setLogin(String login) {
        Browser.getInstance().type(loginInput, login);
        return this;
    }

    public LoginPage setPassword(String password) {
        Browser.getInstance().type(passwordInput, password);
        return this;
    }

    public void signIn() {
        Browser.getInstance().highlightAndClick(loginButton);
    }

    public String getErrorMessage() {
        return Browser.getInstance().getText(errorMessage);
    }

    public boolean hasErrorMessage() {
        return Browser.getInstance().isVisible(errorMessage);
    }
}
