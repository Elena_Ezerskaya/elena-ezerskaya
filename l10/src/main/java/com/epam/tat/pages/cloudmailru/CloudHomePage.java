package com.epam.tat.pages.cloudmailru;

import com.epam.tat.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.File;

public class CloudHomePage extends AbstractPage {
    private static final String CREATE_BUTTON_LOCATOR = "//div[@id='cloud_toolbars']"
            + "//div[@data-bem='b-dropdown' and @data-group='create']";
    private static final String CREATE_FOLDER_LOCATOR = "//div[@id='toolbar-left']//a[@data-name='folder']";
    private static final String NEW_FOLDER_NAME_LOCATOR = "//input[@type='text' and @value='Новая папка']";
    private static final String ADD_BUTTON_LOCATOR = "//button[@data-name='add']";
    private static final String CONFIRM_DELETE_BUTTON_LOCATOR = "//button[@data-name='remove']";
    private static final String CLOSE_DELETE_BUTTON_LOCATOR = "//div[@class='layer_trashbin-tutorial']"
            + "//button[@data-name='close']";
    private static final String SWITCH_TO_TRASHBIN_BUTTON_LOCATOR = "//div[@class='layer_trashbin-tutorial']"
            + "//button[@data-name='trashbin']";
    private static final String DELETE_BUTTON_LOCATOR = "//div[@id='cloud_toolbars']//div[@data-name='remove']";
    private static final String UPLOAD_BUTTON_LOCATOR = "//div[@id='cloud_toolbars']//div[@data-name='upload']";
    private static final String SELECT_FILES_BUTTON_LOCATOR = "//input[@type='file' and @class='drop-zone__input']";

    @FindBy(xpath = UPLOAD_BUTTON_LOCATOR)
    private WebElement uploadButton;

    @FindBy(xpath = SWITCH_TO_TRASHBIN_BUTTON_LOCATOR)
    private WebElement switchToTrashbinButton;

    @FindBy(xpath = CLOSE_DELETE_BUTTON_LOCATOR)
    private WebElement closeDeleteButton;

    @FindBy(xpath = CONFIRM_DELETE_BUTTON_LOCATOR)
    private WebElement confirmDeleteButton;

    @FindBy(xpath = DELETE_BUTTON_LOCATOR)
    private WebElement deleteButton;

    @FindBy(xpath = ADD_BUTTON_LOCATOR)
    private WebElement addButton;

    @FindBy(xpath = CREATE_BUTTON_LOCATOR)
    private WebElement createButton;

    @FindBy(xpath = CREATE_FOLDER_LOCATOR)
    private WebElement createFolder;

    @FindBy(xpath = NEW_FOLDER_NAME_LOCATOR)
    private WebElement newFolderName;

    public CloudHomePage(WebDriver driver) {
        super(driver);
    }

    public CloudHomePage clickOnCreateButton() {
        highlightAndClick(waitForAppear(createButton));
        return this;
    }

    public CloudHomePage chooseCreateFolder() {
        highlightAndClick(createFolder);
        return this;
    }

    public CloudDataPage nameNewFolder(String folderName) {
        newFolderName.sendKeys(folderName);
        highlightAndClick(addButton);
        return new CloudDataPage(driver);
    }

    public CloudTrashbinPage deleteSelectedAndGetTrashbin() {
        highlightAndClick(waitForAppear(deleteButton));
        highlightAndClick(waitForAppear(confirmDeleteButton));
        highlightAndClick(waitForAppear(switchToTrashbinButton));
        return new CloudTrashbinPage(driver);
    }

    public CloudDataPage uploadFile(String fileName) {
        highlightAndClick(uploadButton);
        driver.findElement(By.xpath(SELECT_FILES_BUTTON_LOCATOR))
                .sendKeys(new File(fileName).getAbsolutePath());
        return new CloudDataPage(driver);
    }
}
