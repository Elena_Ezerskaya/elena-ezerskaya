package com.epam.gomel.homework;

import org.mockito.Mockito;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

@Listeners(TestListener.class)
public class GirlTest {

    static final double MIN_RICH_LIMIT = 1000000;

    private Boy mockedNotRichBoy;
    private Boy mockedRichBoy;

    @BeforeClass(description = "Creates mocked Boy objects: mockedRichBoy and mockedNotReachBoy")
    public void setUp() {
        mockedRichBoy = mock(Boy.class);
        when(mockedRichBoy.isRich()).thenReturn(true);

        mockedNotRichBoy = mock(Boy.class);
        when(mockedNotRichBoy.isRich()).thenReturn(false);
    }

    @Test(description = "Checks setter for isPretty", priority = 0)
    public void setPrettyTest() throws Exception {
        Girl girl = new Girl();
        assertFalse(girl.isPretty());
        girl.setPretty(true);
        assertTrue(girl.isPretty());
    }

    @Test(description = "Boyfriend spends money if he is rich, else he doesn't spend money",
            dependsOnGroups = "rich_boyfriend")
    public void spendBoyFriendMoneyTest() {
        Girl girl = new Girl(true, false, new Boy(Month.MAY, MIN_RICH_LIMIT));
        girl.spendBoyFriendMoney(MIN_RICH_LIMIT / 2 + 1);
        assertThat(girl.getBoyFriend().getWealth(), is(MIN_RICH_LIMIT / 2 - 1));
        girl.spendBoyFriendMoney(1);
        assertThat(girl.getBoyFriend().getWealth(), is(MIN_RICH_LIMIT / 2 - 1));
    }

    @Test(description = "If boyfriend is rich should return true, if boyfriend is not rich - false",
            groups = {"rich_boyfriend", "boyfriend_buy_slim_rich"})
    public void isBoyfriendRichTest() {
        Girl girl = new Girl(false, false, mockedRichBoy);
        assertEquals(girl.isBoyfriendRich(), true);
        Mockito.verify(mockedRichBoy).isRich();

        girl = new Girl(true, false, mockedNotRichBoy);
        assertEquals(girl.isBoyfriendRich(), false);
        Mockito.verify(mockedNotRichBoy).isRich();
    }

    @Test(description = "True if girl is pretty(girl.isPretty=true) and boyfriend is rich(mockedRichBoy)",
            dependsOnGroups = "rich_boyfriend", groups = "boyfriend_buy_slim_rich")
    public void isBoyFriendWillBuyNewShoesTest() {
        Girl girl = new Girl(true, true, mockedRichBoy);
        assertTrue(girl.isBoyFriendWillBuyNewShoes());
    }

    @Test(description = "True if girl.isPretty = false and girl.isSlimFriendGotAFewKilos = true; False in other cases",
            groups = "boyfriend_buy_slim_rich")
    public void isSlimFriendBecameFatTest() {
        assertTrue(new Girl(false, true).isSlimFriendBecameFat());
        assertFalse(new Girl(false, false).isSlimFriendBecameFat());
        assertFalse(new Girl(true, true).isSlimFriendBecameFat());
        assertFalse(new Girl(true, false).isSlimFriendBecameFat());
    }

    @Test(description = "Checks mood of girl", dependsOnGroups = "boyfriend_buy_slim_rich", groups = "mood")
    public void getMoodTest() {
        assertEquals(new Girl(true, false, mockedRichBoy).getMood(), Mood.EXCELLENT);
        assertEquals(new Girl(true, false, mockedNotRichBoy).getMood(), Mood.GOOD);
        assertEquals(new Girl(false, false, mockedRichBoy).getMood(), Mood.GOOD);
        assertEquals(new Girl(false, true, mockedNotRichBoy).getMood(), Mood.NEUTRAL);
        assertEquals(new Girl(false, false, mockedNotRichBoy).getMood(), Mood.I_HATE_THEM_ALL);
    }
}
