package com.epam.tat.tests;

import com.epam.tat.framework.listeners.TestListener;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.product.mailru.mail.bo.Letter;
import com.epam.tat.product.mailru.mail.bo.LetterFactory;
import com.epam.tat.product.mailru.commonbo.User;
import com.epam.tat.product.mailru.commonbo.UserFactory;
import com.epam.tat.product.mailru.mail.service.AuthentificationService;
import com.epam.tat.product.mailru.mail.service.FoldersService;
import com.epam.tat.product.mailru.mail.service.LetterService;
import org.testng.Assert;
import org.testng.annotations.*;

@Listeners(TestListener.class)
public class MailRUTest {

    private static final String LETTER_IS_SENT_CONFIRMATION = "Ваше письмо отправлено. Перейти во Входящие";
    private static final String INCORRECT_ADDRESS_ALERT_ER_MESSAGE =
            "В поле «Кому» указан некорректный адрес получателя.\n"
            + "Исправьте ошибку и отправьте письмо ещё раз.";
    private static final String LETTER_IS_SAVED_CONFIRMATION = "Сохранено в черновиках в.*";

    private LetterService letterService;
    private FoldersService foldersService;

    @BeforeMethod
    private void setUp() {
        Browser browser = Browser.getInstance();
        AuthentificationService authentificationService = new AuthentificationService();
        User user = UserFactory.getUserWithCorrectCredentials();
        authentificationService.loginEx(user);
        letterService = new LetterService();
        foldersService = new FoldersService();
    }

    @Test(description = "Check sending letter with all correctly filled fields")
    public void composeCorrectLetterAndSend() {
        Letter letter = LetterFactory.getLetterWithCorrectToSubjectBody();
        letterService.composeLetter(letter);
        Assert.assertEquals(letterService.sendLetter(letter), LETTER_IS_SENT_CONFIRMATION, "Letter is not sent!");
    }

    @Test(description = "Check sending letter without subject and body")
    public void composeLetterWithoutSubjectAndBodyAndSend() {
        Letter letter = LetterFactory.getLetterWithoutSubjectAndBody();
        letterService.composeLetter(letter);
        Assert.assertEquals(letterService.sendLetter(letter), LETTER_IS_SENT_CONFIRMATION, "Letter is not sent!");
    }

    @Test(description = "Check sending letter without properly filled address ")
    public void composeLetterWithInvalidFieldToAndTryToSend() {
        Letter letter = LetterFactory.getLetterWithInvalidFieldTo();
        letterService.composeLetter(letter);
        Assert.assertEquals(letterService.sendLetter(letter),
                INCORRECT_ADDRESS_ALERT_ER_MESSAGE, "Invalid error message!");
    }

    @Test(description = "Check that letter with all correctly filled fields is present in Inbox and Sent folders")
    public void checkThatLetterWithAllCorrectlyFilledFieldsIsPresentInInboxAndSentFolders() {
        Letter letter = LetterFactory.getLetterWithCorrectToSubjectBody();
        letterService.composeLetter(letter);
        letterService.sendLetter(letter);
        Assert.assertTrue(foldersService.isLetterPresentInInboxAndSent(letter),
                "Letter is not present in Inbox and Sent folders!");
    }

    @Test(description = "Check that letter without subject and body is present in Inbox and Sent folders")
    public void checkThatLetterWithoutSubjectAndBodyIsPresentInInboxAndSentFolders() {
        Letter letter = LetterFactory.getLetterWithoutSubjectAndBody();
        letterService.composeLetter(letter);
        letterService.sendLetter(letter);
        Assert.assertTrue(foldersService.isLetterPresentInInboxAndSent(letter),
                "Letter is not present in Inbox and Sent folders!");
    }

    @Test(description = "Check that draft mail is deleted from Drafts and Trash folders")
    public void composeDraftMailAndPermanentlyDeleteIt() {
        Letter letter = LetterFactory.getLetterWithCorrectToSubjectBody();
        letterService.composeLetter(letter);
        Assert.assertTrue(letterService.saveLetter(letter).matches(LETTER_IS_SAVED_CONFIRMATION),
                "Draft mail is not deleted!");
        Assert.assertTrue(foldersService.deleteLetterFromDraftAndTrash(letter), "Draft mail is not deleted!");
    }

    @AfterMethod
    public void killDriver() {
        Browser.getInstance().stopBrowser();
    }

}
