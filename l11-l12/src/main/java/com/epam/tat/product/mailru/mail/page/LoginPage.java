package com.epam.tat.product.mailru.mail.page;

import com.epam.tat.framework.ui.Browser;
import org.openqa.selenium.By;

public class LoginPage {
    private static final String URL = "https://mail.ru/";

    private static final By loginInput = By.id("mailbox__login");
    private static final By passwordInput = By.id("mailbox__password");
    private static final By loginButton = By.id("mailbox__auth__button");
    private static final By errorMessage = By.id("mailbox:authfail");

    public LoginPage open() {
        Browser.getInstance().navigate(URL);
        //getWebdriver().get(URL);
        return this;
    }

    public LoginPage setLogin(String login) {
        Browser.getInstance().type(loginInput, login);
        //loginInput.sendKeys(login);
        return this;
    }

    public LoginPage setPassword(String password) {
        Browser.getInstance().type(passwordInput, password);
        //passwordInput.sendKeys(password);
        return this;
    }

    public void signIn() {
        Browser.getInstance().highlightAndClick(loginButton);
        //loginButton.click();
    }

    public String getErrorMessage() {
        return Browser.getInstance().getText(errorMessage);
        //return errorMessage.getText();
    }

    public boolean hasErrorMessage() {
        return Browser.getInstance().isVisible(errorMessage);
    }
}
