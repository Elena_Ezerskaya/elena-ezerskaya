package com.epam.tat.product.mailru.mail.page;

import com.epam.tat.framework.ui.Browser;
import org.openqa.selenium.By;

public class NewLetterPage {
    private static final By composeLetter = By.xpath("//a[@data-name='compose']");
    private static final By addressToField = By.xpath("//textarea[@data-original-name ='To']");
    private static final By subjectField = By.name("Subject");
    private static final By mailBody = By.id("tinymce");
    private static final By sendButton = By.xpath("//div[@data-name = 'send']");
    private static final By saveButton = By.xpath("//div[@data-name = 'saveDraft']");
    private static final By saveMessage = By.xpath("//*[@id='b-toolbar__right']//div[@data-mnemo='saveStatus']");
    private static final By mailBodyIframe = By.xpath("//iframe[contains(@id,'compose')]");
    private static final By composePopUpLocator =
            By.xpath("//div[@class='is-compose-empty_in']//button[@type='submit']");

    public NewLetterPage open() {
        Browser.getInstance().highlightAndClick(composeLetter);
        return this;
    }

    public NewLetterPage setFieldTo(String to) {
        Browser.getInstance().type(addressToField, to);
        return this;
    }

    public NewLetterPage setSubject(String subject) {
        Browser.getInstance().type(subjectField, subject);
        return this;
    }

    public NewLetterPage setBody(String body) {
        Browser.getInstance().switchToFrame(mailBodyIframe);
        Browser.getInstance().clear(mailBody);
        Browser.getInstance().type(mailBody, body);
        Browser.getInstance().switchToDefaultContent();
        return this;
    }

    public void sendLetter() {
        Browser.getInstance().highlightAndClick(sendButton);
    }

    public void saveLetter() {
        Browser.getInstance().highlightAndClick(saveButton);
    }

    public boolean isSaved() {
        return Browser.getInstance().isVisible(saveMessage);
    }

    public void confirmSendingLetterWithEmptyBody() {
        Browser.getInstance().highlightAndClick(composePopUpLocator);
    }

    public boolean hasAlert() {
        return Browser.getInstance().isAlertPresent();
    }

    public String getLetterIsSavedConfirmation() {
        return Browser.getInstance().getText(saveMessage);
    }

    public String getErrorMessage() {
        return Browser.getInstance().getAlertText();
    }

}
