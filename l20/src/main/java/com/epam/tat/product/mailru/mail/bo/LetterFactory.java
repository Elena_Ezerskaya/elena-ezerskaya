package com.epam.tat.product.mailru.mail.bo;

import com.epam.tat.framework.utils.TempDataUtils;

public class LetterFactory {

    private static final String CORRECT_EMAIL = "elenkha2017@mail.ru";

    public static Letter getLetterWithCorrectToSubjectBody() {
        return new Letter(CORRECT_EMAIL,
                "Test_letter_" + TempDataUtils.getUniqueString(),
                "Hi, Mr " + TempDataUtils.getUniqueString() + "!");
    }

    public static Letter getLetterWithoutSubjectAndBody() {
        return new Letter(CORRECT_EMAIL, "", "");
    }

    public static Letter getLetterWithInvalidFieldTo() {
        return new Letter("random" + TempDataUtils.getUniqueString()
                + "mail.com", "subject", "Hi!");
    }
}
