package com.epam.tat;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        //Test string for input of arguments
        // String str = "triangle:3 circle:6 square:4 rectangle:5:6 square:16 triangle:5";

        List<Shape> shapes = new ArrayList<>();

        for (String arg : args) {
            shapes.add(ShapeUtils.parse(arg));
        }

        for (Shape shape : shapes) {
            System.out.println(shape.toString());
        }
    }
}
