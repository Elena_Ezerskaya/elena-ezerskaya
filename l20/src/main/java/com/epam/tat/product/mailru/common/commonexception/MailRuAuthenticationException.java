package com.epam.tat.product.mailru.common.commonexception;

public class MailRuAuthenticationException extends Error {
    public MailRuAuthenticationException(String message) {
        super(message);
    }
}
