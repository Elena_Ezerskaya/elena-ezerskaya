package com.epam.tat.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;

public class FoldersPage extends AbstractPage {

    private static final String EXACT_SUBJECT_LETTER_LOCATOR_PREFIX =
            "//*[@id='b-letters']//div[@data-cache-key and not(contains(@style,'display'))]//a[@data-subject='";
    private static final String EXACT_SUBJECT_LETTER_LOCATOR_POSTFIX = "']";
    private static final String EXACT_SUBJECT_LETTER_CHECKBOX_LOCATOR_POSTFIX = "/div[1]";
    private static final String EMPTY_SUBJECT_LETTER_LOCATOR_PREFIX =
            "//*[@id='b-letters']//div[@data-cache-key "
            + "and not(contains(@style,'display'))]"
            + "//div[@data-id][1]//a[not(@data-subject) or @data-subject='<Без темы>']";
    private static final String REMOVE_BUTTON_LOCATOR
            = "//*[@id='b-toolbar__right']/div[not(contains(@style, 'display: none;'))]//div[@data-name='remove']";

    @FindBy(xpath = REMOVE_BUTTON_LOCATOR)
    private WebElement removeButton;

    public FoldersPage(WebDriver driver) {
        super(driver);
    }

    public FoldersPage(WebDriver driver, String dataId) {
        super(driver);
        waitForPresence(By.xpath("//*[@id='b-letters']//div[@data-cache-key='"
                + dataId
                + "_undefined_false' and not(contains(@style,'display: none'))]"));
    }

    public boolean isLetterExist(String subject) {
        try {
            return waitForPresence(By.xpath(EXACT_SUBJECT_LETTER_LOCATOR_PREFIX
                    + subject
                    + EXACT_SUBJECT_LETTER_LOCATOR_POSTFIX)).isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean isLetterExist() {
        try {
            return waitForPresence(By.xpath(EMPTY_SUBJECT_LETTER_LOCATOR_PREFIX)).isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public FoldersPage selectLetter(String subject) {
        try {
            waitForPresence(By.xpath(EXACT_SUBJECT_LETTER_LOCATOR_PREFIX
                    + subject
                    + EXACT_SUBJECT_LETTER_LOCATOR_POSTFIX
                    + EXACT_SUBJECT_LETTER_CHECKBOX_LOCATOR_POSTFIX)).click();
        } catch (NoSuchElementException e) {
            return null;
        }
        return this;
    }

    public FoldersPage deleteSelectedLetter() {
        removeButton.click();
        return this;
    }
}
