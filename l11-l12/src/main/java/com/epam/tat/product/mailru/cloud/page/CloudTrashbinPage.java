package com.epam.tat.product.mailru.cloud.page;

import com.epam.tat.framework.ui.Browser;
import org.openqa.selenium.By;

public class CloudTrashbinPage {
    private static final By clearTrashbinButton = By.xpath("//*[@id='toolbar']//div[@data-name='clear']");
    private static final  By confirmClearTrashbinButton = By.xpath("//button[@data-name='empty']");
    private static final By folderMessage = By.xpath("//*[@id='datalist']/div[1]/div/div/div");
    private static final By foldersCount = By.xpath("//*[@id='breadcrumbs']/div/span");

    public CloudTrashbinPage clearTrashbin() {
        Browser.getInstance().highlightAndClick(clearTrashbinButton);
        Browser.getInstance().highlightAndClick(confirmClearTrashbinButton);
        return this;
    }

    public boolean isFolderEmpty() {
        return Browser.getInstance().isVisibleWithWait(folderMessage)
                && !Browser.getInstance().isPresent(foldersCount);
    }
}
