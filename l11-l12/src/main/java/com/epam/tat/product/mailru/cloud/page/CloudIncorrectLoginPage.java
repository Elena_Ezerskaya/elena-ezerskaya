package com.epam.tat.product.mailru.cloud.page;

import com.epam.tat.framework.ui.Browser;
import org.openqa.selenium.By;

public class CloudIncorrectLoginPage {
    public static final By enterTheMailIframe = By.xpath("//*[@id='auth-form']//iframe");
    public static final By errormessage = By.xpath("//div[@class='b-login__errors']");

    public boolean hasLoginIframe() {
        return Browser.getInstance().isVisible(enterTheMailIframe);
    }

    public String getErrorMessage() {
        Browser.getInstance().switchToFrame(enterTheMailIframe);
        String errorMessage = Browser.getInstance().getText(errormessage);
        Browser.getInstance().switchToDefaultContent();
        return errorMessage;
    }
}
