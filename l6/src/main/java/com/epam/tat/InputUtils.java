package com.epam.tat;

import com.epam.tat.shape.Shape;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class InputUtils {

    public static Set<Shape> getShapesSet(String[] args) throws IllegalArgumentException, IOException {
        switch (args[0]) {
            case "--plain":
                return getShapesFromArgs(args);
            case "--path":
                return getShapesFromFile(args[1]);
            default:
                throw new IllegalArgumentException("Wrong input");
        }
    }

    public static Set<Shape> getShapesFromArgs(String[] args) {
        Set<Shape> shapeSet = new HashSet<>();
        for (int i = 1; i < args.length; i++) {
            shapeSet.add(ShapeUtils.parse(args[i]));
        }
        return shapeSet;
    }

    public static Set<Shape> getShapesFromFile(String fileName) throws IOException {
        Set<Shape> shapeSet = new HashSet<>();
        File file = new File(fileName);
        if (!file.exists() || file.isDirectory()) {
            throw new IOException("File " + fileName + " does not exist");
        }
        List<String> shapesFromFile = FileUtils.readLines(file, Charset.defaultCharset());
        for (String shape : shapesFromFile) {
            shapeSet.add(ShapeUtils.parse(shape));
        }
        return shapeSet;
    }
}
