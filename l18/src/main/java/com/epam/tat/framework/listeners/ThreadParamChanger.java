package com.epam.tat.framework.listeners;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.runner.Parameters;
import org.testng.IAlterSuiteListener;
import org.testng.xml.XmlSuite;

import java.util.List;

public class ThreadParamChanger implements IAlterSuiteListener {
    @Override
    public void alter(List<XmlSuite> suites) {
        for (XmlSuite suite : suites) {
            if (Parameters.getInstance().getParallelMode() != null) {
                suite.setThreadCount(Parameters.getInstance().getThreadCount());
                suite.setParallel(Parameters.getInstance().getParallelMode());
            }
            Log.info(String.format("*** Suite %s: parallel=%s, thread-count=%d",
                    suite.getName(),
                    suite.getParallel().toString(),
                    suite.getThreadCount()));
        }
    }
}
