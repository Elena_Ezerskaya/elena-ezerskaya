package com.epam.tat.mailru;

import com.epam.tat.pages.TopPage;
import com.epam.tat.pages.cloudmailru.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.annotations.Test;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.epam.tat.DriverTimeouts.IMPLICIT_WAIT;
import static com.epam.tat.DriverTimeouts.PAGE_LOAD;

public class CloudMailRuTest {
    private static final String CLOUD_MAIL_RU = "https://cloud.mail.ru/";
    private static final String EMAIL = "elenkha2017@mail.ru";
    private static final String CORRECT_PASS = "ktyrfgtyrf71";

    private WebDriver driver;
    private String folderName;
    private String fileName;

    @BeforeClass
    private void setUpDriver() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        fileName = "upload.txt";
    }

    @BeforeMethod
    private void setUpForLogin() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT.getValue(), TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD.getValue(), PAGE_LOAD.getUnit());
        driver.get(CLOUD_MAIL_RU);
        new CloudStartPage(driver).enterLoginPage()
                .login(EMAIL, CORRECT_PASS)
                .getCurrentUserEmail();
        folderName = "F_" + UUID.randomUUID().toString();
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void loginToMailWithCorrectPassword() {
        Assert.assertEquals(new TopPage(driver)
            .getCurrentUserEmail(), EMAIL, "Invalid result of login.");
    }

    @Test
    public void createNewFolderAndCheckItCreated() {
        Assert.assertTrue(new CloudHomePage(driver)
            .clickOnCreateButton()
            .chooseCreateFolder()
            .nameNewFolder(folderName)
            .isFolderExists(folderName), "New folder is not created.");
    }

    @Test
    public void removeFolderAndCheckItDoesNotExist() {
        CloudHomePage cloudHomePage = new CloudHomePage(driver);
        cloudHomePage.clickOnCreateButton()
            .chooseCreateFolder()
            .nameNewFolder(folderName)
            .selectFolder(folderName);
        Assert.assertTrue(cloudHomePage.deleteSelectedAndGetTrashbin()
            .clearTrashbin()
            .isFolderEmpty(), "Folder is not remove.");
    }

    @Test
    public void uploadFileToRootandCheckItUploaded() {
        Assert.assertTrue(new CloudHomePage(driver)
            .uploadFile(fileName)
            .isFolderExists(fileName), "File is not uploaded.");
    }

    @Test
    public void checkThatUploadedFileWasReplacedInNewFolder() {
        CloudHomePage cloudHomePage = new CloudHomePage(driver);
        cloudHomePage.clickOnCreateButton()
            .chooseCreateFolder()
            .nameNewFolder(folderName)
            .isFolderExists(folderName);
        Assert.assertTrue(cloudHomePage.uploadFile(fileName)
                .dragAndDropFileToFolder(fileName, folderName)
                .clickMoveConformationButton()
                .clickOnDataElement(folderName)
                .isFolderExists(folderName + '/' + fileName), "Uploaded file is not replaced.");
    }

    @Test
    public void createShareAndCheckIt() {
        driver.get(new CloudHomePage(driver)
                .uploadFile(fileName)
                .contextClickOnElement(fileName)
                .clickOnGetShareLink()
                .getPublicUrl());
        Assert.assertTrue(new CloudSharedPage(driver).isSharedItemPresense(fileName), "Shared link is not opened.");
    }

    @AfterClass
    public void killDriver() {
        driver.quit();
    }
}
