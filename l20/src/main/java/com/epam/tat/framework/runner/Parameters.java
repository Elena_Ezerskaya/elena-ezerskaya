package com.epam.tat.framework.runner;

import com.beust.jcommander.Parameter;
import com.epam.tat.framework.ui.BrowserType;

public class Parameters {
    private static Parameters instance;

    @Parameter(names = {"--rdriver", "-r"}, description = "Use remote driver")
    private Boolean driverRemote = false;

    @Parameter(names = {"--rhost"}, description = "Remote web driver host")
    private String remoteDriverHost = "127.0.0.1";

    @Parameter(names = {"--rport"}, description = "Remote web driver port")
    private String remoteDriverPort = "4444";

    @Parameter(names = {"--driver", "-d"}, description = "Path to Google Chrome driver")
    private String pathToDriver = "src/main/resources/webdrivers/chromedriver.exe";

    @Parameter(names = {"--log4j", "-l"}, description = "Path to log4j.propeties")
    private String log4j = "src/main/resources/settings/log4j.properties";

    @Parameter(names = {"--browser", "-b"}, description = "Browser type")
    private BrowserType browserType = BrowserType.CHROME;

    @Parameter(names = {"--help", "-h"}, help = true, description = "How to use")
    private boolean help;

    public static synchronized Parameters getInstance() {
        if (instance == null) {
            instance = new Parameters();
        }
        return instance;
    }

    public boolean isHelp() {
        return help;
    }

    public String getLog4j() {
        return log4j;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public String getPathToDriver() {
        return pathToDriver;
    }

    public Boolean isDriverRemote() {
        return driverRemote;
    }

    public String getRemoteDriverHost() {
        return remoteDriverHost;
    }

    public String getRemoteDriverPort() {
        return remoteDriverPort;
    }
}
