package com.epam.tat.product.mailru.cloud.service;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.utils.TempDataUtils;
import com.epam.tat.product.mailru.cloud.bo.Folder;
import com.epam.tat.product.mailru.cloud.exception.CloudDataException;
import com.epam.tat.product.mailru.cloud.page.CloudDataPage;
import com.epam.tat.product.mailru.cloud.page.CloudHomePage;

import java.io.IOException;

public class CloudDataService {

    public boolean  createFolder(Folder folder) {
        CloudDataPage cloudDataPage = new CloudHomePage()
                .clickOnCreateButton()
                .chooseCreateFolder()
                .setFolderName(folder.getName())
                .addFolder();
        if (!cloudDataPage.isFolderExists(folder.getName())) {
            throw new CloudDataException("Is not created!");
        } else {
            return true;
        }
    }

    public boolean removeFolder(Folder folder) {
        new CloudDataPage().selectFolder(folder.getName());
        return new CloudHomePage().clickOnDeleteButton()
                .confirmDeletion()
                .switchToTrashbin()
                .clearTrashbin()
                .isFolderEmpty();
    }

    public boolean uploadFile(String fileName) {
        try {
            TempDataUtils.createFile(fileName);
        } catch (IOException e) {
            throw new CloudDataException("Temp file is not created");
        }
        CloudDataPage cloudDataPage = new CloudHomePage()
                .clickOnUploadButton()
                .uploadFile(fileName);
        if  (!cloudDataPage.isFolderExists(fileName)) {
            throw new CloudDataException("Is not uploaded!");
        }
        TempDataUtils.deleteFile(fileName);
        return true;
    }

    public boolean dragAndDropFileToFolder(String fileName, Folder folder) {
        CloudDataPage cloudDataPage = new CloudDataPage()
                .dragAndDropFileToFolder(fileName, folder.getName())
                .clickMoveConformationButton()
                .clickOnDataElement(folder.getName());
        if (!cloudDataPage.isFolderExists(folder.getName() + '/' + fileName)) {
            throw new CloudDataException("Is not replaced!");
        } else {
            return true;
        }
    }

    public void shareLinkToElement(String fileName) {
        CloudDataPage cloudDataPage = new CloudDataPage();
        String url = cloudDataPage.contextClickOnElementToShare(fileName)
                .clickOnGetShareLink()
                .getPublicUrl();
        Browser.getInstance().getWrappedDriver().get(url);
    }
}
