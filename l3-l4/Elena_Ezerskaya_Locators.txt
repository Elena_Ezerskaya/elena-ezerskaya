XPath and CSS locators to web elements:

Google search page :
- query input
//input[@id='lst-ib']
input#lst-ib

- �search in Google� button
//input[@type='submit' and @name='btnK']
input[type='submit'][name='btnK']

- �I am lucky!� button
//input[@type='submit' and @name='btnI']
input[type='submit'][name='btnI']

- Search for �nyan cat� and create a locator that returns ALL 10 result links (see screenshot)
//div[@id='rso']//h3/a
div#rso h3>a

- n-th �o� letter in Goooooooooogle   (n = 1...10)
please use digit from 1..10 instead of N in following locators
//table[@id='nav']/tbody//td[N+1]//span
//*[@id='nav']/tbody//td[position()>=2 and position() <= 11][N]//span
table#nav>tbody td:nth-child(N+1) span

Follwing xpath select all 10th 'o' in sequence
//table[@id='nav']/tbody//td[position()>=2 and position() <= 11]//span � all 10 letters �o�



Mail.ru login page:
- login input
//input[@id='mailbox__login']
input#mailbox__login

- password input
//input[@id='mailbox__password']
input#mailbox__password

- �Enter� button
//input[@id='mailbox__auth__button']
input#mailbox__auth__button



Mail.ru main page (logged in):
-links to folders (incoming, outcoming, spam, deleted, drafts)

- inbox
//*[@id='b-nav_folders']//a[@href='/messages/inbox/']//span[2]
div#b-nav_folders a[href='/messages/inbox/']>span:nth-child(4)

- sent
//*[@id='b-nav_folders']//a[@href='/messages/sent/']//span[1]
div#b-nav_folders a[href='/messages/sent/']>span:nth-child(3)

� drafts
//*[@id='b-nav_folders']//a[@href='/messages/drafts/']//span[1]
div#b-nav_folders a[href='/messages/drafts/']>span:nth-child(3)

� spam
//*[@id='b-nav_folders']//a[@href='/messages/spam/']//span[2]
div#b-nav_folders a[href='/messages/spam/']>span:nth-child(4)

� trash
//*[@id='b-nav_folders']//a[@href='/messages/trash/']//span[2]
div#b-nav_folders a[href='/messages/trash/']>span:nth-child(4)


-action buttons (write new letter, delete, mark as spam, mark as not spam, mark as read, move to another folder)

� compose
//div[@id='b-toolbar__left']//a[@data-name="compose"]/span   
or
//a[@data-name="compose"]/span
div#b-toolbar__left a[data-name="compose"]>span
or
a[data-name="compose"]>span

� delete
//div[@id='b-toolbar__right']//div[@data-name="remove"]/span   
or
//div[@data-name="remove"]/span
div#b-toolbar__right div[data-name='remove']>span   
or
div[data-name='remove']>span

- spam
//div[@id='b-toolbar__right']/div[@style=""]//div[@data-name="spam"]/span
div#b-toolbar__right>div[style=""] div[data-name="spam"]>span

- not spam
//div[@id='b-toolbar__right']//div[@data-name="noSpam"]/span 
or
//div[@data-name="noSpam"]/span
div#b-toolbar__right div[data-name="noSpam"]>span    
or
div[data-name="noSpam"]>span

- mark as read
mark first unread letter in the list as read.
//*[@id='b-letters']//div[@data-id and .//div[contains(@class,"_unread")]][1]//a//div[@data-id and @data-act="unread"]
div#b-letters div:nth-child(1)>div>a[data-name="link"] div[class*="_unread"][data-id]
mark Nth unread letter in the list as read.
//*[@id='b-letters']//div[@data-id and .//div[contains(@class,"_unread")]][N]//a//div[@data-id and @data-act="unread"]
div#b-letters div:nth-child(N)>div>a[data-name="link"] div[class*="_unread"][data-id]

- moveTo
//*[@id='b-toolbar__right']//div[@data-group='moveTo']/div/span
div#b-toolbar__right div[data-group='moveTo']>div>span

- Checkbox for one exact letter
//*[@id='b-letters']//div[@data-id][1]//a/div[1] point to the first letter in the list
//*[@id='b-letters']//div[@data-id][N]//a/div[1] where N is number of letter in the list
div#b-letters div[data-id]:nth-child(1) a>div:nth-child(1)

- Opening link for one exact letter
//*[@id='b-letters']//div[@data-id][1]//a
div#b-letters div[data-id]:nth-child(1) a


- New letter page: inputs for address, topic, text, file attach

- new letter page
//div[@id='b-compose']   
or  
//*[@id='b-compose']
div#b-compose
or
[id=b-compose]

- input for address
//textarea[@data-original-name="To"]
textarea[data-original-name=To]

- topic
//input[@name="Subject"]
input[name=Subject]
or
[name=Subject]

- text
//body[@id='tinymce']  
body#tinymce

file attach
//input[@name="Filedata"]
input[name=Filedata]   
or     
[name=Filedata]
