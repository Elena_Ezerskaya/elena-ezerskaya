package com.epam.tat.pages.cloudmailru;

import com.epam.tat.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class CloudDataPage extends AbstractPage {
    private static final String FOLDER_LOCATOR_PREFIX = "//div[@id='datalist']//div[@data-name='link' and @data-id='/";
    private static final String FOLDER_LOCATOR_POSTFIX = "']";
    private static final String FOLDER_CHECKBOX_LOCATOR_POSTFIX = "//div[@class='b-checkbox__box']";
    private static final String CONFIRM_MOVE_BUTTON_LOCATOR = "//button[@data-name='move']";
    private static final String GET_PUBLISH_LINK_LOCATOR = "//a[@data-name='publish']";
    private static final String PUBLISH_LINK_URL_LOCATOR = "//div[@class='publishing__url']/input[@data-name='url']";

    public CloudDataPage(WebDriver driver) {
        super(driver);
    }

    public String getPublicUrl() {
        return waitForAppear(By.xpath(PUBLISH_LINK_URL_LOCATOR)).getAttribute("value");
    }

    public CloudDataPage clickOnGetShareLink() {
        highlightAndClick(waitForPresence(By.xpath(GET_PUBLISH_LINK_LOCATOR)));
        return this;
    }

    public CloudDataPage clickMoveConformationButton() {
        highlightAndClick(waitForPresence(By.xpath(CONFIRM_MOVE_BUTTON_LOCATOR)));
        return this;
    }

    public WebElement getDataElement(String name) {
        return waitForAppear(By.xpath(FOLDER_LOCATOR_PREFIX + name + FOLDER_LOCATOR_POSTFIX));
    }

    public CloudDataPage clickOnDataElement(String name) {
        highlightAndClick(getDataElement(name));
        return this;
    }

    public boolean isFolderExists(String folderName) {
        return getDataElement(folderName).isDisplayed();
    }

    public CloudDataPage selectFolder(String folderName) {
        highlightAndClick(waitForAppear(By.xpath(FOLDER_LOCATOR_PREFIX
                + folderName + FOLDER_LOCATOR_POSTFIX
                + FOLDER_CHECKBOX_LOCATOR_POSTFIX)));
        return this;
    }

    public CloudDataPage dragAndDropFileToFolder(String fileName, String folderName) {
        new Actions(driver)
                .dragAndDrop(getDataElement(fileName),
                        getDataElement(folderName))
                .build()
                .perform();
        return this;
    }

    public CloudDataPage contextClickOnElement(String fileName) {
        new Actions(driver)
                .contextClick(getDataElement(fileName))
                .build()
                .perform();
        return this;
    }
}
