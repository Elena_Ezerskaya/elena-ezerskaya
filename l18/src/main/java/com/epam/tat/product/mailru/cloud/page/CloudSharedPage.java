package com.epam.tat.product.mailru.cloud.page;

import com.epam.tat.framework.ui.Browser;
import org.openqa.selenium.By;

public class CloudSharedPage {

    private static final By sharedItem
            = By.xpath("//div[@id='public-file']/div/div/div/div//a[@data-name='link-action']");

    public boolean isSharedItemPresense(String name) {
        return Browser.getInstance().waitForPresence(sharedItem).getText().equals(name);
    }
}
