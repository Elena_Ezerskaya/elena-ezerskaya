package com.epam.tat.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends AbstractPage {
    private static final String LOGIN_INPUT_LOCATOR = "mailbox__login";
    private static final String PASSWORD_INPUT_LOCATOR = "mailbox__password";
    private static final String LOGIN_BUTTON_SUBMIT_LOCATOR = "mailbox__auth__button";
    private static final String ERROR_MSG_LOCATOR = "mailbox:authfail";

    @FindBy(id = LOGIN_INPUT_LOCATOR)
    private WebElement loginInput;

    @FindBy(id = PASSWORD_INPUT_LOCATOR)
    private WebElement loginPassword;

    @FindBy(id = LOGIN_BUTTON_SUBMIT_LOCATOR)
    private  WebElement loginButton;

    @FindBy(id = ERROR_MSG_LOCATOR)
    private WebElement errorMessage;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public HomePage login(String email, String pass) {
        tryLogin(email, pass);
        return new HomePage(driver);
    }

    public WebElement getIncorrectLoginErrorElement(String email, String pass) {
        tryLogin(email, pass);
        return waitForAppear(errorMessage);
    }

    private void tryLogin(String email, String pass) {
        loginInput.sendKeys(email);
        loginPassword.sendKeys(pass);
        loginButton.click();
    }
}
