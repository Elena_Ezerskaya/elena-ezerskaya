Narrative:
In order to check login functionality
As a user
I want to enter valid and invalid credentials

Scenario: Login to mail.ru with correct credentials
Given Mail.ru login page is open
When I perform login with correct credentials
Then I am logged in

Scenario: Login to mail.ru with incorrect credentials
Given Mail.ru login page is open
When I try to login with incorrect credentials <username>:<password>
Then error message "<errormsg>" is displayed

Examples:
|username|password|errormsg|
|||Введите имя ящика|
|elenkha2017@mail.ru||Введите пароль|
|randomized_value|ktyrfgtyrf71|Неверное имя или пароль|
|elenkha2017@mail.ru|randomized_value|Неверное имя или пароль|

