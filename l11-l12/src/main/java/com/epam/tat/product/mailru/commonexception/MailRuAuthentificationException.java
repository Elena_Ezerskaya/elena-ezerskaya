package com.epam.tat.product.mailru.commonexception;

public class MailRuAuthentificationException extends Error {

    //public MailRuAuthentificationException() {
    //}

    public MailRuAuthentificationException(String message) {
        super(message);
    }
}
