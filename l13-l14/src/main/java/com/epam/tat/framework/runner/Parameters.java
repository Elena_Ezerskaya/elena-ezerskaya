package com.epam.tat.framework.runner;

import com.beust.jcommander.Parameter;
import com.epam.tat.framework.ui.BrowserType;
import org.testng.xml.XmlSuite;

import java.util.List;

public class Parameters {
    private static Parameters instance;

    @Parameter(names = {"--suites", "-s"}, required = true, description = "Comma separated paths to test suites")
    private List<String> suite;

    @Parameter(names = {"--driver", "-d"}, description = "Path to Google Chrome driver")
    private String pathToDriver = "chromedriver.exe";

    @Parameter(names = {"--log4j", "-l"}, description = "Path to log4j.propeties")
    private String log4j = "log4j.properties";

    @Parameter(names = {"--browser", "-b"}, description = "Browser type")
    private BrowserType browserType = BrowserType.CHROME;

    @Parameter(names = {"--tread-count", "-t"},
            description = "Thread count. Note: "
                    + "Thread count will be set for all suites if option [--parallel|-p] is defined.")
    private Integer treadCount = 1;

    @Parameter(names = {"--parallel", "-p"},
            description = "Parallel mode. Note: "
                    + "Parallel will be set for all suites if option is defined.")
    private XmlSuite.ParallelMode parallelMode;

    @Parameter(names = {"--help", "-h"}, help = true, description = "How to use")
    private boolean help;

    public static synchronized Parameters getInstance() {
        if (instance == null) {
            instance = new Parameters();
        }
        return instance;
    }

    public boolean isHelp() {
        return help;
    }

    public String getLog4j() {
        return log4j;
    }

    public List<String> getSuite() {
        return suite;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public Integer getTreadCount() {
        return treadCount;
    }

    public String getPathToDriver() {
        return pathToDriver;
    }

    public XmlSuite.ParallelMode getParallelMode() {
        return parallelMode;
    }
}
