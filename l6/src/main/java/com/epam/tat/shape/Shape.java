package com.epam.tat.shape;

public interface Shape {

    int perimeter();

    int area();
}
