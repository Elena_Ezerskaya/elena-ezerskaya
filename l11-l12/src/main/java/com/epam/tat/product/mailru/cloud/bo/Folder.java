package com.epam.tat.product.mailru.cloud.bo;

public class Folder {
    private String name;

    public Folder(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
