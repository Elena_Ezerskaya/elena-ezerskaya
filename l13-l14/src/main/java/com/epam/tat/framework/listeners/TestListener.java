package com.epam.tat.framework.listeners;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.ui.Browser;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.internal.IResultListener2;

public class TestListener implements IResultListener2 {

    private void logITestResult(String onAction, ITestResult result) {
        Log.info(String.format("[%s] %s.%s", onAction,
                result.getTestClass().getRealClass().getSimpleName(),
                result.getName()));
    }

    @Override
    public void onTestStart(ITestResult result) {
        logITestResult("start", result);
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        logITestResult("success", result);
        Browser.getInstance().screenshot();
        Browser.getInstance().stopBrowser();
    }

    @Override
    public void onTestFailure(ITestResult result) {
        logITestResult("failure", result);
        Browser.getInstance().screenshot();
        Browser.getInstance().stopBrowser();
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        logITestResult("skip", result);
        Browser.getInstance().stopBrowser();
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        logITestResult("failure%", result);
        Browser.getInstance().screenshot();
        Browser.getInstance().stopBrowser();
    }

    @Override
    public void onStart(ITestContext context) {
        Log.info(String.format("[TEST STARTED] %s", context.getName()));
    }

    @Override
    public void onFinish(ITestContext context) {
        Log.info(String.format("[TEST FINISHED] %s", context.getName()));
    }

    @Override
    public void beforeConfiguration(ITestResult itr) {
        logITestResult("before", itr);
    }

    @Override
    public void onConfigurationSuccess(ITestResult itr) {
        logITestResult("configSuccess", itr);
        if (itr.getName().equals("prepareCloudMailRuTest")) {
            Browser.getInstance().screenshot();
            Browser.getInstance().stopBrowser();
        }
    }

    @Override
    public void onConfigurationFailure(ITestResult itr) {
        logITestResult("configFailure", itr);
        Browser.getInstance().screenshot();
        Browser.getInstance().stopBrowser();
    }

    @Override
    public void onConfigurationSkip(ITestResult itr) {
        logITestResult("configSkip", itr);
    }
}
