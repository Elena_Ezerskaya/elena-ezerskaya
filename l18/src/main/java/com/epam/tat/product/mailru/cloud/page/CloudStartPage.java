package com.epam.tat.product.mailru.cloud.page;

import com.epam.tat.framework.ui.Browser;
import org.openqa.selenium.By;

public class CloudStartPage {

    private static final String URL = "https://cloud.mail.ru/";

    private static final By enterCloudButton = By.xpath("//input[@type='button' and @value='Войти в Облако']");

    public CloudStartPage open() {
        Browser.getInstance().navigate(URL);
        return this;
    }

    public CloudLoginPage openLoginPage() {
        Browser.getInstance().highlightAndClick(enterCloudButton);
        return new CloudLoginPage();
    }
}
