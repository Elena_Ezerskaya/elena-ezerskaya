package com.epam.tat.product.mailru.cloud.bo;

import com.epam.tat.framework.utils.TempDataUtils;

import java.io.File;

public class FileFactory {
    public static File getFile() {
        return new File("File_" + TempDataUtils.getUniqueString() + ".txt");
    }

}
