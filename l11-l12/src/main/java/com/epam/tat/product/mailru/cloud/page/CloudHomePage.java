package com.epam.tat.product.mailru.cloud.page;

import com.epam.tat.framework.ui.Browser;
import org.openqa.selenium.By;

public class CloudHomePage {
    private static final By createButton = By.xpath("//div[@id='cloud_toolbars']"
            + "//div[@data-bem='b-dropdown' and @data-group='create']");
    private static final By createFolder = By.xpath("//div[@id='toolbar-left']//a[@data-name='folder']");
    private static final By newFolderName = By.xpath("//input[@type='text' and @value='Новая папка']");
    private static final By addButton = By.xpath("//button[@data-name='add']");
    private static final By confirmDeleteButton = By.xpath("//button[@data-name='remove']");
    private static final By closeDeleteButton = By.xpath("//div[@class='layer_trashbin-tutorial']"
            + "//button[@data-name='close']");
    private static final By switchToTrashbinButton = By.xpath("//div[@class='layer_trashbin-tutorial']"
            + "//button[@data-name='trashbin']");
    private static final By deleteButton = By.xpath("//div[@id='cloud_toolbars']//div[@data-name='remove']");
    private static final By uploadButton = By.xpath("//div[@id='cloud_toolbars']//div[@data-name='upload']");
    private static final By selectFileInput = By.xpath("//input[@type='file' and @class='drop-zone__input']");

    public CloudHomePage clickOnCreateButton() {
        Browser.getInstance().highlightAndClick(createButton);
        return this;
    }

    public CloudHomePage chooseCreateFolder() {
        Browser.getInstance().highlightAndClick(createFolder);
        return this;
    }

    public CloudHomePage setFolderName(String folderName) {
        Browser.getInstance().type(newFolderName, folderName);
        return this;
    }

    public CloudDataPage addFolder() {
        Browser.getInstance().highlightAndClick(addButton);
        return new CloudDataPage();
    }

    public CloudHomePage clickOnDeleteButton() {
        Browser.getInstance().highlightAndClick(deleteButton);
        return this;
    }

    public CloudHomePage confirmDeletion() {
        Browser.getInstance().highlightAndClick(confirmDeleteButton);
        return this;
    }

    public CloudTrashbinPage switchToTrashbin() {
        Browser.getInstance().highlightAndClick(switchToTrashbinButton);
        return new CloudTrashbinPage();
    }

    public CloudHomePage clickOnUploadButton() {
        Browser.getInstance().highlightAndClick(uploadButton);
        return this;
    }

    public CloudDataPage uploadFile(String fileName) {
        Browser.getInstance().uploadFile(fileName, selectFileInput);
        return new CloudDataPage();
    }

}
