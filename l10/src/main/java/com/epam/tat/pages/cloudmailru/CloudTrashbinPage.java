package com.epam.tat.pages.cloudmailru;

import com.epam.tat.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CloudTrashbinPage extends AbstractPage {
    private static final String CLEAR_TRASHBIN_BUTTON_LOCATOR = "//*[@id='toolbar']//div[@data-name='clear']";
    private static final String CONFIRM_CLEAR_TRASHBIN_BUTTON_LOCATOR = "//button[@data-name='empty']";
    private static final String FOLDER_MESSAGE_LOCATOR = "//*[@id='datalist']/div[1]/div/div/div";
    private static final String FOLDERS_COUNT_LOCATOR = "//*[@id='breadcrumbs']/div/span";

    @FindBy(xpath = CONFIRM_CLEAR_TRASHBIN_BUTTON_LOCATOR)
    private WebElement confirmClearTrashbinbutton;

    @FindBy(xpath = CLEAR_TRASHBIN_BUTTON_LOCATOR)
    private WebElement clearTrashbinButton;

    public CloudTrashbinPage(WebDriver driver) {
        super(driver);
    }

    public CloudTrashbinPage clearTrashbin() {
        highlightAndClick(waitForAppear(clearTrashbinButton));
        highlightAndClick(waitForAppear(confirmClearTrashbinbutton));
        return this;
    }

    public boolean isFolderEmpty() {
        return waitForAppear(By.xpath(FOLDER_MESSAGE_LOCATOR)).isDisplayed()
                && !isPresence(FOLDERS_COUNT_LOCATOR);
    }
}
