package com.epam.tat;

public class ShapeUtils {

    public static Shape parse(String s) {

        String[] parsedShape = s.split(":");

        switch (ShapeType.valueOf(parsedShape[0].toUpperCase())) {
            case TRIANGLE:
                return new Triangle(Integer.parseInt(parsedShape[1]));
            case RECTANGLE:
                return new Rectangle(Integer.parseInt(parsedShape[1]), Integer.parseInt(parsedShape[2]));
            case CIRCLE:
                return new Circle(Integer.parseInt(parsedShape[1]));
            case SQUARE:
                return new Square(Integer.parseInt(parsedShape[1]));
            default:
                return null;
        }
    }

}
