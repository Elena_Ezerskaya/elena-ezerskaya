package com.epam.tat.product.mailru.mail.service;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.product.mailru.mail.bo.Letter;
import com.epam.tat.product.mailru.mail.exception.MailRuLetterException;
import com.epam.tat.product.mailru.mail.page.NewLetterPage;
import com.epam.tat.product.mailru.mail.page.LetterHasBeenSentPage;

public class LetterService {

    public void composeLetter(Letter letter) {
        Log.info(String.format("LetterService.composeLetter: %s",
                letter.toString()));
        new NewLetterPage()
                .open()
                .setFieldTo(letter.getTo())
                .setSubject(letter.getSubject())
                .setBody(letter.getBody());
    }

    public String sendLetter(Letter letter) {
        Log.info(String .format("LetterService.sendLetter: %s", letter.toString()));
        NewLetterPage newLetterPage = new NewLetterPage().sendLetter();
        if (newLetterPage.hasAlert()) {
            throw new MailRuLetterException(newLetterPage.getErrorMessage());
        }
        if (letter.getBody().equals("")) {
            newLetterPage.confirmSendingLetterWithEmptyBody();
        }
        LetterHasBeenSentPage letterHasBeenSentPage = new LetterHasBeenSentPage();
        if (letterHasBeenSentPage.isSent()) {
            return letterHasBeenSentPage.getLetterSendingConfirmation();
        }
        throw new MailRuLetterException("Is not sent!");
    }

    public String saveLetter() {
        Log.info("LetterService.savedLetter");
        NewLetterPage newLetterPage = new NewLetterPage().saveLetter();
        if (newLetterPage.isSaved()) {
            return newLetterPage.getLetterIsSavedConfirmation();
        }
        throw new MailRuLetterException("Is not saved!");
    }
}
