package com.epam.tat;

import com.epam.tat.shape.Shape;
import java.util.*;

public class Main {

    public static void main(String[] args) throws Exception {
        //Test string for input
        // --path C:\common\Elena_epam\git\elena-ezerskaya\l6\shapes.txt
        //--plain triangle:3 square:4 circle:5 triangle:4 square:4 circle:5 rectangle:2:4 rectangle:4:2 rectangle:2:8

        Set<Shape> shapeSet = InputUtils.getShapesSet(args);

        List<Shape> shapeList = new ArrayList<>(shapeSet);

        AreaComparator areaComparator = new AreaComparator();
        Collections.sort(shapeList, areaComparator);

        ShapeUtils.printShapes(shapeList);
    }
}
