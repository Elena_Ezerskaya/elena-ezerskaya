package com.epam.tat.product.mailru.cloud.service;

import com.epam.tat.product.mailru.cloud.page.CloudIncorrectLoginPage;
import com.epam.tat.product.mailru.cloud.page.CloudLoginPage;
import com.epam.tat.product.mailru.cloud.page.CloudStartPage;
import com.epam.tat.product.mailru.commonbo.User;
import com.epam.tat.product.mailru.commonpage.HeaderBar;
import com.epam.tat.product.mailru.commonexception.MailRuAuthentificationException;

public class CloudLoginService {

    public String login(User user) {
        CloudLoginPage cloudLoginPage = new CloudStartPage()
                .open()
                .enterLoginPage()
                .setLogin(user.getEmail())
                .setPassword(user.getPassword())
                .signIn();
        if (cloudLoginPage.hasFormInputError()) {
            return cloudLoginPage.getFormInputErrorMessage();
        }
        CloudIncorrectLoginPage cloudIncorrectLoginPage = new CloudIncorrectLoginPage();
        if (cloudIncorrectLoginPage.hasLoginIframe()) {
            return cloudIncorrectLoginPage.getErrorMessage();
        }
        HeaderBar headerBar = new HeaderBar();
        if (headerBar.isLogin()) {
            return headerBar.getCurrentUserEmail();
        }
        return "Is not logged in!";
    }

    public void loginEx(User user) {
        String loginResult = login(user);
        if (!loginResult.equals(user.getEmail())) {
            throw new MailRuAuthentificationException(loginResult);
        }
    }
}
