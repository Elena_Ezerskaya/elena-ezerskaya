package com.epam.tat.pages.mailru;

import com.epam.tat.pages.AbstractPage;
import com.epam.tat.pages.TopPage;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;

public class FoldersPage extends AbstractPage {

    private static final String CREATE_NEW_LETTER_LOCATOR = "//a[@data-name='compose']";
    private static final String INBOX_LOCATOR = "//*[@id='b-nav_folders']//a[@href='/messages/inbox/']";
    private static final String SENT_LOCATOR = "//*[@id='b-nav_folders']//a[@href='/messages/sent/']";
    private static final String DRAFTS_LOCATOR = "//*[@id='b-nav_folders']//a[@href='/messages/drafts/']";
    private static final String TRASH_LOCATOR = "//*[@id='b-nav_folders']//a[@href='/messages/trash/']";
    private static final String EXACT_SUBJECT_LETTER_LOCATOR_PREFIX =
            "//*[@id='b-letters']//div[@data-cache-key and not(contains(@style,'display'))]//a[@data-subject='";
    private static final String EXACT_SUBJECT_LETTER_LOCATOR_POSTFIX = "']";
    private static final String EXACT_SUBJECT_LETTER_CHECKBOX_LOCATOR_POSTFIX = "/div[1]";
    private static final String EMPTY_SUBJECT_LETTER_LOCATOR_PREFIX =
            "//*[@id='b-letters']//div[@data-cache-key "
                    + "and not(contains(@style,'display'))]"
                    + "//div[@data-id][1]//a[not(@data-subject) or @data-subject='<Без темы>']";
    private static final String REMOVE_BUTTON_LOCATOR
            = "//*[@id='b-toolbar__right']/div[not(contains(@style, 'display: none;'))]//div[@data-name='remove']";

    @FindBy(xpath = CREATE_NEW_LETTER_LOCATOR)
    private WebElement composeLetter;

    @FindBy(xpath = INBOX_LOCATOR)
    private WebElement inboxLink;

    @FindBy(xpath = SENT_LOCATOR)
    private WebElement sentLink;

    @FindBy(xpath = DRAFTS_LOCATOR)
    private WebElement draftsLink;

    @FindBy(xpath = TRASH_LOCATOR)
    private WebElement trashLink;

    @FindBy(xpath = REMOVE_BUTTON_LOCATOR)
    private WebElement removeButton;

    public FoldersPage(WebDriver driver) {
        super(driver);
    }

    public FoldersPage(WebDriver driver, String dataId) {
        super(driver);
        waitForPresence(By.xpath("//*[@id='b-letters']//div[@data-cache-key='"
                + dataId
                + "_undefined_false' and not(contains(@style,'display: none'))]"));
    }

    public TopPage getTopPage() {
        return new TopPage(driver);
    }

    public boolean isLetterExist(String subject) {
        try {
            return waitForPresence(By.xpath(EXACT_SUBJECT_LETTER_LOCATOR_PREFIX
                    + subject
                    + EXACT_SUBJECT_LETTER_LOCATOR_POSTFIX)).isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean isLetterExist() {
        try {
            return waitForPresence(By.xpath(EMPTY_SUBJECT_LETTER_LOCATOR_PREFIX)).isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public FoldersPage selectLetter(String subject) {
        try {
            waitForPresence(By.xpath(EXACT_SUBJECT_LETTER_LOCATOR_PREFIX
                    + subject
                    + EXACT_SUBJECT_LETTER_LOCATOR_POSTFIX
                    + EXACT_SUBJECT_LETTER_CHECKBOX_LOCATOR_POSTFIX)).click();
        } catch (NoSuchElementException e) {
            return null;
        }
        return this;
    }

    public FoldersPage deleteSelectedLetter() {
        removeButton.click();
        return this;
    }

    public NewLetterPage getNewLetterPage() {
        composeLetter.click();
        return new NewLetterPage(driver);
    }

    public FoldersPage getInboxPage() {
        waitForAppear(inboxLink).click();
        return new FoldersPage(driver, "0");
    }

    public FoldersPage getSentPage() {
        waitForAppear(sentLink).click();
        return new FoldersPage(driver, "500000");
    }

    public FoldersPage getDraftsPage() {
        waitForAppear(draftsLink).click();
        return new FoldersPage(driver, "500001");
    }

    public FoldersPage getTrashPage() {
        driver.navigate().refresh();
        waitForAppear(trashLink).click();
        return new FoldersPage(driver, "500002");
    }
}

