package com.epam.tat.tests;

import com.epam.tat.product.mailru.cloud.bo.FileFactory;
import com.epam.tat.product.mailru.cloud.bo.Folder;
import com.epam.tat.product.mailru.cloud.bo.FolderFactory;
import com.epam.tat.product.mailru.cloud.service.CloudDataService;
import com.epam.tat.product.mailru.cloud.service.CloudLoginService;
import com.epam.tat.product.mailru.common.commonbo.User;
import com.epam.tat.product.mailru.common.commonbo.UserFactory;
import com.epam.tat.product.mailru.cloud.page.CloudSharedPage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.io.File;

public class CloudMailRuTest {

    private static final String FOLDER_CONTAINS_NOTHING_MESSAGE = "В этой папке нет содержимого";
    private CloudDataService cloudDataService = new CloudDataService();

    @BeforeClass (description = "Clear cloud before all tests run")
    void prepareCloudMailRuTest() {
        CloudLoginService cloudLoginService = new CloudLoginService();
        User user = UserFactory.getUserWithCorrectCredentials();
        cloudLoginService.login(user);
        cloudDataService.clearCloud();
    }

    @BeforeMethod
    private void setUp() {
        CloudLoginService cloudLoginService = new CloudLoginService();
        User user = UserFactory.getUserWithCorrectCredentials();
        cloudLoginService.login(user);
    }

    @Test(description = "Create new folder and check that it created")
    public void createNewFolderAndCheckItCreated() {
        Folder folder = FolderFactory.getFolder();
        cloudDataService.createFolder(folder);
    }

    @Test(description = "Check that removed folder does not exist")
    public void removeFolderAndCheckItDoesNotExist() {
        Folder folder = FolderFactory.getFolder();
        cloudDataService.createFolder(folder);
        Assert.assertEquals(cloudDataService.removeFolder(folder),
                FOLDER_CONTAINS_NOTHING_MESSAGE, "Folder is not removed!");
    }

    @Test(description = "Upload file to root directory and check that it is uploaded")
    public void uploadFileToRootAndCheckItUploaded() {
        File file = FileFactory.getFile();
        cloudDataService.uploadFile(file);
    }

    @Test(description = "Check that uploaded file is replaced to folder")
    public void checkThatUploadedFileIsReplacedInNewFolder() {
        Folder folder = FolderFactory.getFolder();
        File file = FileFactory.getFile();
        cloudDataService.createFolder(folder);
        cloudDataService.uploadFile(file);
        cloudDataService.dragAndDropFileToFolder(file, folder);
    }

    @Test(description = "Share link, check that link opens and displays shared element")
    public void createShareAndCheckIt() {
        File file = FileFactory.getFile();
        cloudDataService.uploadFile(file);
        cloudDataService.shareLinkToElement(file.getName());
        Assert.assertTrue(new CloudSharedPage().isSharedItemPresense(file.getName()), "Shared link is not opened!");
    }
}
