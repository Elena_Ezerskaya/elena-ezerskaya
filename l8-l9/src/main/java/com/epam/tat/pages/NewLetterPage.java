package com.epam.tat.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewLetterPage extends AbstractPage {
    private static final String ADDRESS_TO_LOCATOR = "//textarea[@data-original-name ='To']";
    private static final String SUBJECT_LOCATOR = "//input[@name ='Subject']";
    private static final String MAIL_BODY_LOCATOR = "//body[@id='tinymce']";
    private static final String SEND_LOCATOR = "//div[@data-name = 'send']";
    private static final String SAVE_BUTTON_LOCATOR = "//div[@data-name = 'saveDraft']";
    private static final String SAVE_MESSAGE_LOCATOR = "//*[@id='b-toolbar__right']//div[@data-mnemo='saveStatus']";
    private static final String MAIL_BODY_IFRAME_LOCATOR = "//iframe[contains(@id,'compose')]";
    private static final String COMPOSE_POPUP_LOCATOR = "//div[@class='is-compose-empty_in']//button[@type='submit']";

    @FindBy(xpath = ADDRESS_TO_LOCATOR)
    private WebElement addressToField;

    @FindBy(xpath = SUBJECT_LOCATOR)
    private WebElement subjectField;

    @FindBy(xpath = MAIL_BODY_LOCATOR)
    private WebElement mailBody;

    @FindBy(xpath = SEND_LOCATOR)
    private WebElement sendButton;

    @FindBy(xpath = SAVE_BUTTON_LOCATOR)
    private WebElement saveButton;

    @FindBy(xpath = SAVE_MESSAGE_LOCATOR)
    private WebElement saveMessage;

    @FindBy(xpath = MAIL_BODY_IFRAME_LOCATOR)
    private WebElement mailBodyIframe;

    @FindBy(xpath = COMPOSE_POPUP_LOCATOR)
    private WebElement composePopUpLocator;

    public NewLetterPage(WebDriver driver) {
        super(driver);
    }

    public void composeLetter(String to) {
        addressToField.sendKeys(to);
    }

    public void composeLetter(String to, String subject, String text) {
        composeLetter(to);
        waitForAppear(subjectField).sendKeys(subject);
        new WebDriverWait(driver, ELEMENT_VISIBILITY_TIMEOUT_SECONDS)
                .until(ExpectedConditions
                        .frameToBeAvailableAndSwitchToIt(By.xpath(MAIL_BODY_IFRAME_LOCATOR)));
        waitForAppear(By.xpath(MAIL_BODY_LOCATOR)).sendKeys(text);
        driver.switchTo().defaultContent();
    }

    public NewLetterPage sendLetter(String to, String subject, String text) {
        composeLetter(to, subject, text);
        sendButton.click();
        return this;
    }

    public boolean wasLetterSent() {
        return new LetterWasSentPage(driver).wasLetterSent();
    }

    public NewLetterPage sendLetterWithoutSubjectAndBody(String to) {
        composeLetter(to);
        sendButton.click();
        waitForPresence(By.xpath(COMPOSE_POPUP_LOCATOR)).click();
        return this;
    }

    public boolean sendLetterWithoutProperlyFieldAddress(String to) {
        composeLetter(to);
        sendButton.click();
        return !driver.switchTo().alert().getText().isEmpty();
    }

    public boolean wasLetterSaved() {
        return waitForAppear(saveMessage).isDisplayed();
    }

    public NewLetterPage saveLetter(String to, String subject, String text) {
        composeLetter(to, subject, text);
        saveButton.click();
        return this;
    }
}
