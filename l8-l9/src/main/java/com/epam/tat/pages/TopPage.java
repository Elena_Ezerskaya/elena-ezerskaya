package com.epam.tat.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TopPage extends AbstractPage {

    public static final String USER_NAME_LABEL_LOCATOR = "//i[@id='PH_user-email']";

    @FindBy(xpath = USER_NAME_LABEL_LOCATOR)
    private WebElement userNameLabel;

    public TopPage(WebDriver driver) {
        super(driver);
    }

    public String getCurrentUserEmail() {
        return waitForAppear(userNameLabel).getText();
    }
}
