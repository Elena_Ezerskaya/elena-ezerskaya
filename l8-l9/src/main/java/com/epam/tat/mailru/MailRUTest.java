package com.epam.tat.mailru;

import com.epam.tat.pages.HomePage;
import com.epam.tat.pages.LoginPage;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.epam.tat.DriverTimeouts.IMPLICIT_WAIT;
import static com.epam.tat.DriverTimeouts.PAGE_LOAD;

public class MailRUTest {
    private static final String INCORRECT_EMAIL = "incorrect";
    private static final String INCORRECT_PASS = "incorrect";
    private static final String EMAIL = "elenkha2017@mail.ru";
    private static final String CORRECT_PASS = "ktyrfgtyrf71";
    private static final String MAIL_RU = "https://mail.ru/";
    private static final String MAIL_TEXT = "Hi!";

    private WebDriver driver;
    private String mailSubject;

    @BeforeClass
    private void setUpDriver() {
        System.setProperty("webdriver.chrome.driver", "BrowserDrivers\\chromedriver.exe");
        mailSubject = "Test_letter_" + UUID.randomUUID().toString();
    }

    @BeforeMethod
    private void setUpForLogin() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT.getValue(), TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD.getValue(), PAGE_LOAD.getUnit());
        driver.get(MAIL_RU);
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void loginToMailWithCorrectPassword() {
        Assert.assertEquals(new LoginPage(driver)
                        .login(EMAIL, CORRECT_PASS)
                        .getTopPage()
                        .getCurrentUserEmail(), EMAIL);
    }

    @Test
    public void tryLoginToMailWithIncorrectPassword() {
        Assert.assertTrue(new LoginPage(driver)
                .getIncorrectLoginErrorElement(EMAIL, INCORRECT_PASS)
                .isDisplayed());
    }

    @Test
    public void composeAndSendLetter() {
        Assert.assertTrue(new LoginPage(driver)
                .login(EMAIL, CORRECT_PASS)
                .getNewLetterPage()
                .sendLetter(EMAIL, mailSubject, MAIL_TEXT)
                .wasLetterSent());
    }

    @Test
    public void checkSentLetterIsPresentInInboxAndSentFolders() {
        new LoginPage(driver)
                .login(EMAIL, CORRECT_PASS)
                .getNewLetterPage()
                .sendLetter(EMAIL, mailSubject, MAIL_TEXT)
                .wasLetterSent();
        HomePage homePage = new HomePage(driver);
        Assert.assertTrue(
                homePage.getInboxPage()
                .isLetterExist(mailSubject)
                &
                homePage.getSentPage()
                .isLetterExist(mailSubject));
    }

    @Test
    public void composeAndSendLetterWithoutSubjectAndBody() {
        Assert.assertTrue(new LoginPage(driver)
                .login(EMAIL, CORRECT_PASS)
                .getNewLetterPage()
                .sendLetterWithoutSubjectAndBody(EMAIL)
                .wasLetterSent());
    }

    @Test
    public void checkSentLetterWithoutSubjectAndBodyIsPresentInInboxAndSentFolders() {
        new LoginPage(driver)
                .login(EMAIL, CORRECT_PASS)
                .getNewLetterPage()
                .sendLetterWithoutSubjectAndBody(EMAIL)
                .wasLetterSent();
        HomePage homePage = new HomePage(driver);
        Assert.assertTrue(homePage.getInboxPage()
                .isLetterExist()
                &
                homePage.getSentPage()
                .isLetterExist()
        );
    }

    @Test
    public void composeAndTryToSendLetterWithoutProperlyFieldAddress() {
        Assert.assertTrue(new LoginPage(driver)
                .login(EMAIL, CORRECT_PASS)
                .getNewLetterPage()
                .sendLetterWithoutProperlyFieldAddress(INCORRECT_EMAIL)
        );
    }

    @Test
    public void composeDraftMailAndPermanentlyDeleteIt() {
        boolean result = new LoginPage(driver)
                .login(EMAIL, CORRECT_PASS)
                .getNewLetterPage()
                .saveLetter(EMAIL, mailSubject, MAIL_TEXT)
                .wasLetterSaved();

        new HomePage(driver).getDraftsPage()
                .selectLetter(mailSubject)
                .deleteSelectedLetter();

        new HomePage(driver).getTrashPage()
                .selectLetter(mailSubject)
                .deleteSelectedLetter();
        result &= !new HomePage(driver).getTrashPage().isPresence(By.xpath(mailSubject));
        Assert.assertTrue(result);
    }

    @AfterClass
    public void killDriver() {
        driver.quit();
    }
}
