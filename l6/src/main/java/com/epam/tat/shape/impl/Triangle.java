package com.epam.tat.shape.impl;

import com.epam.tat.shape.AbstractShape;

public class Triangle extends AbstractShape {
    static final int TRIANGLEAREAFORMULACONST_1 = 3;
    static final int TRIANGLEAREAFORMULACONST_2 = 4;

    public Triangle(int a) {
        super(a);
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int area() {
        return  (int) (Math.sqrt(TRIANGLEAREAFORMULACONST_1) * Math.pow(a, 2)) / TRIANGLEAREAFORMULACONST_2;
    }

    public int perimeter() {
        return TRIANGLEAREAFORMULACONST_1 * a;
    }

    public String toString() {
        return "Triangle - " + a + "\n\t"
                + "Area: " + area() + "\n\t"
                + "Perimeter: " + perimeter();
    }
}
