package com.epam.tat.product.mailru.mail.service;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.product.mailru.mail.bo.Letter;
import com.epam.tat.product.mailru.mail.page.FoldersPage;

public class FoldersService {

    public boolean isLetterPresentInInboxAndSent(Letter letter) {
        Log.info(String.format("FolderService.isLetterPresentInInboxAndSent: %s", letter.toString()));
        FoldersPage foldersPage = new FoldersPage();
        foldersPage.switchToInbox();
        if (!isLetterExist(letter)) {
            return false;
        }
        foldersPage.switchToSent();
        return isLetterExist(letter);
    }

    public boolean isLetterExist(Letter letter) {
        Log.info(String.format("FolderService.isLetterExist: %s", letter.toString()));
        if (letter.getSubject().equals("")) {
            return new FoldersPage().isLetterExist();
        } else {
            return new FoldersPage().isLetterExist(letter.getSubject());
        }
    }

    public boolean deleteLetterFromDraftAndTrash(Letter letter) {
        Log.info(String.format("FolderService.deleteLetterFromDraftAndTrash: %s", letter.toString()));
        FoldersPage foldersPage = new FoldersPage();
        return  foldersPage.switchToDrafts()
                .selectLetter(letter.getSubject())
                .deleteSelectedLetter()
                .switchToTrash()
                .selectLetter(letter.getSubject())
                .deleteSelectedLetter()
                .isLetterNotExist(letter.getSubject());
    }
}
