package com.epam.tat.product.mailru.mail.service;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.product.mailru.common.commonpage.HeaderBar;
import com.epam.tat.product.mailru.common.commonbo.User;
import com.epam.tat.product.mailru.common.commonexception.MailRuAuthenticationException;
import com.epam.tat.product.mailru.mail.page.LoginPage;

public class AuthenticationService {

    public String login(User user) {
        Log.info(String.format("AuthenticationService.login: %s", user.toString()));
        LoginPage loginPage = new LoginPage().open();
        loginPage.setLogin(user.getEmail())
                .setPassword(user.getPassword())
                .signIn();
        if (loginPage.hasErrorMessage()) {
            Log.debug("AuthenticationService.login: has error message");
            throw new MailRuAuthenticationException(loginPage.getErrorMessage());
        }
        HeaderBar headerBar = new HeaderBar();
        if (headerBar.checkUserIsLoggedIn()) {
            return headerBar.getCurrentUserEmail();
        }
        throw new MailRuAuthenticationException("Is not logged in!");
    }
}
