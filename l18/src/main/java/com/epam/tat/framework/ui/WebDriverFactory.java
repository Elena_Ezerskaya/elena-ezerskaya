package com.epam.tat.framework.ui;

import com.epam.tat.framework.runner.Parameters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class WebDriverFactory {
    public static WebDriver getWebDriver() {
        if (Parameters.getInstance().isDriverRemote()) {
            return getRemoteWebDriver();
        }
        WebDriver webDriver;
        switch (Parameters.getInstance().getBrowserType()) {
            case CHROME:
                System.setProperty("webdriver.chrome.driver", Parameters.getInstance().getPathToDriver());
                webDriver = new ChromeDriver();
                break;
            case FIREFOX:
                System.setProperty("webdriver.gecko.driver", Parameters.getInstance().getPathToDriver());
                webDriver = new FirefoxDriver();
                break;
            default:
                throw new RuntimeException("No support for: " + Parameters.getInstance().getBrowserType());
        }
        return  webDriver;
    }

    public static WebDriver getRemoteWebDriver() {
        DesiredCapabilities capabilities;
        switch (Parameters.getInstance().getBrowserType()) {
            case CHROME:
                capabilities = DesiredCapabilities.chrome();
                break;
            case FIREFOX:
                capabilities = DesiredCapabilities.firefox();
                break;
            default:
                throw new RuntimeException("No support for: " + Parameters.getInstance().getBrowserType());
        }

        try {
            return new RemoteWebDriver(new URL(String.format("http://%s:%s/wd/hub",
                    Parameters.getInstance().getRemoteDriverHost(),
                    Parameters.getInstance().getRemoteDriverPort())), capabilities);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Wrong remote web driver URL: "
                    + Parameters.getInstance().getRemoteDriverHost());
        }
    }
}
