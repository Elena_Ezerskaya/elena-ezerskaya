package com.epam.tat;

import com.epam.tat.shape.Shape;

import java.util.Comparator;

public class AreaComparator implements Comparator<Shape> {

    @Override
    public int compare(Shape o1, Shape o2) {
        if (o1.area() < o2.area()) {
            return 1;
        } else if (o1.area() > o2.area()) {
            return -1;
        } else {
            // sort (desc) by Class name in case equal areas
            return o2.getClass().getName().compareTo(o1.getClass().getName());
        }
    }
}

