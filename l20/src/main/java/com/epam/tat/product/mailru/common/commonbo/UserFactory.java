package com.epam.tat.product.mailru.common.commonbo;

import com.epam.tat.framework.utils.TempDataUtils;

public class UserFactory {
    private static final String CORRECT_EMAIL = "elenkha2017@mail.ru";
    private static final String CORRECT_PASSWORD = "ktyrfgtyrf71";
    private static final String RANDOMIZED_VALUE = "randomized_value";

    public static String getUsername(String userName) {
        if (userName.equals(RANDOMIZED_VALUE)) {
            return "random" + TempDataUtils.getUniqueString() + "mail.com";
        } else {
            return userName;
        }
    }

    public static String getPassword(String password) {
        if (password.equals(RANDOMIZED_VALUE)) {
            return "pass" + TempDataUtils.getUniqueString();
        } else {
            return password;
        }
    }

    public static User getUserWithCorrectCredentials() {
        return new User(CORRECT_EMAIL, CORRECT_PASSWORD);
    }

}
