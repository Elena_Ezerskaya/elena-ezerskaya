package com.epam.tat.pages.mailru;

import com.epam.tat.pages.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LetterWasSentPage extends AbstractPage {

    private static final String LETTER_WAS_SENT_LOCATOR = "b-compose__sent";

    @FindBy(id = LETTER_WAS_SENT_LOCATOR)
    private WebElement letterWasSentConfirmation;

    public LetterWasSentPage(WebDriver driver) {
        super(driver);
    }

    public boolean wasLetterSent() {
        return isVisible(waitForAppear(letterWasSentConfirmation));
    }
}
